<!-- Footer -->
<footer>
  <div class="container">
    <?php
    $column_style = get_theme_mod('footer_columns', '3,3,6');
    $columns_list = explode(",", $column_style);
    $countColumns = 1;
    ?>

    <div class="row">
      <?php foreach ($columns_list as $value): ?>
        <div class="col-md-<?php echo $value; ?> col-sm-<?php echo $value; ?>">
          <?php if (is_active_sidebar('cfieldtheme-footer' . $countColumns)) : ?>
            <?php dynamic_sidebar('cfieldtheme-footer' . $countColumns); ?>
          <?php endif; ?>
        </div>
        <?php ++$countColumns; ?>
      <?php endforeach; ?>
    </div>
  </div>
  <div class="footer-bottom">
    <div class="container">
      <div class="row">
        <div class="col-xs-6"> &copy;
          2015 <?php if (date('Y') != '2015'): ?><?php echo ' - ' . date('Y'); ?><?php endif; ?> <?php bloginfo('name'); ?> <?php if (get_theme_mod(
              'is_under_construction'
          )): ?>|
            <span class="text-danger bigger-110 orange"><i class="ace-icon fa fa-exclamation-triangle"></i> Sitio en Construcción</span>
          <?php endif; ?>
        </div>
        <div class="col-xs-6 text-right">
          <ul>
            <?php if (get_theme_mod('show_hrg', true)): ?>
              <li><a href="<?php echo get_theme_mod('url_hrg', 'http://www.habanaregeneracion.com'); ?>"><span
                      style="color: #ffffff;">h</span><span
                      style="color: #aac564;">[r]</span><span
                      style="color: #ffffff;">g_arquitectura</span></a></li>
            <?php endif; ?>
          </ul>
        </div>
      </div>
    </div>
  </div>
</footer>

<!-- Go-top Button -->
<div id="go-top"><i class="fa fa-angle-up fa-2x"></i></div>

<?php wp_footer(); ?>

<?php
$post_meta_array = get_post_custom(get_the_ID());

$next_opening = $post_meta_array['cftheme_next_opening_message_show'][0];
?>

<?php if ($next_opening): ?>
  <div class="cf-next-opening">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <?php echo $post_meta_array['cftheme_next_opening_message'][0] ?>
  </div>
<?php endif; ?>
</body>
</html>