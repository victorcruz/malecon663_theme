<?php
/**
 * The template used for displaying page content
 *
 * package     CFieldTheme
 * @subpackage CFieldTheme
 * @since      0.1.0
 */
?>

<?php the_content() ?>