<?php
/**
 * The template for displaying Author bios
 *
 * package     CFieldTheme
 * @subpackage CFieldTheme
 * @since      0.1.0
 */
?>

<section class="blog-author clearfix">
  <h3>
    <?php _cftheme_e( 'About the author' ) ?>: <span>
      <a href="<?php echo get_the_author_link() ?>"><?php the_author_meta( 'first_name' ) ?>
      </a>
    </span>
  </h3>
  <img src="<?php echo CFieldTheme::$theme_url.'/images/logo.svg' ?>" alt="Author" class="img-thumbnail"/>

  <p><?php the_author_meta( 'description' ); ?></p>
  <br />
  <a class="author-link" href="<?php echo esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ); ?>"
     rel="author">
    <?php printf( _cftheme__( 'View all posts by %s' ), get_the_author_meta( 'first_name' ) ); ?>
  </a>
</section>