<?php
/*
Template Name: Yeoman Index
*/
?>

<?php

$post_meta_array = get_post_custom(get_the_ID());

$elements = json_decode($post_meta_array['cftheme_index_data'][0], true);
?>

<!doctype html>
<html class="no-js">
<head>
  <meta charset="utf-8">
  <title><?php bloginfo('title'); ?></title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
  <link rel="shortcut icon"
        href="<?php echo get_theme_mod('favicon_image', CFieldTheme::$theme_bower_url . 'favicon.ico'); ?>">
  <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">

  <link rel="stylesheet" href="<?php echo CFieldTheme::$theme_bower_url . 'css/font-awesome.min.css' ?>"/>
  <link rel="stylesheet"
        href="<?php echo CFieldTheme::$theme_url . '/bower_components/bootstrap/dist/css/bootstrap.min.css' ?>"/>
  <link rel="stylesheet" href="<?php echo CFieldTheme::$theme_url . '/bower_components/animate.css/animate.css' ?>"/>

  <link rel="stylesheet" href="<?php echo CFieldTheme::$theme_url . '/app-yeo/styles/main.css' ?>">
</head>
<body>
<!--[if lt IE 10]>
<p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a
    href="http://browsehappy.com/">upgrade
  your browser</a> to improve your experience.</p>
<![endif]-->

<?php
$show_intro = $post_meta_array['cftheme_show_intro'][0];
$show_automatic_intro = $post_meta_array['cftheme_show_automatic_intro'][0];

$page_sound = json_decode($post_meta_array['cftheme_page_sound'][0],
    true);

if ($page_sound['image']['id']) {
  $page_sound_image_url = CFieldTheme_Image::get_img_by_id($page_sound['image']['id']);
}

if ($page_sound['sound']['id']) {
  $page_sound_url = CFieldTheme_Utils::get_url_by_id($page_sound['sound']['id']);
}
?>

<?php if (get_theme_mod('page_sound')): ?>
  <?php if ($page_sound['play_sound']): ?>
    <?php if ($page_sound_url != ''): ?>
      <div id="cf-box-audio-player"
           class="cf-show-audio-player"
           data-autoplay="<?php echo $page_sound['options']['autoplay'] ?>"
           data-automatic_time_close_media="<?php echo $page_sound['options']['automatic_time_close_media'] ?>"
           data-automatic_show_player="<?php echo $page_sound['options']['automatic_show_player'] ?>">
        <i class="fa icon-remove pull-right"></i>

        <div class="cf-div">
          <img src="<?php echo $page_sound_image_url ?>"/>
          <button id="cf-page-audio-play" class="btn btn-xs btn-control">
            <i class="glyphicon glyphicon-play-circle media-player-icon"></i>
          </button>
          <button id="cf-page-audio-pause" class="btn btn-xs btn-control">
            <i class="glyphicon glyphicon-pause media-player-icon"></i>
          </button>
          <audio id="cf-page-audio" src="<?php echo $page_sound_url ?>" loop
                 <?php if ($page_sound['options']['autoplay']): ?>autoplay<?php endif ?>></audio>
        </div>

        <div class="cf-div right">
          <h5><?php echo $page_sound['options']['author_name'] ?></h5>

          <p><?php echo $page_sound['options']['title'] ?></p>
        </div>
      </div>
    <?php endif; ?>
  <?php endif; ?>
<?php endif; ?>

<div id="cf-container-layout" class="container" data-show-intro="<?php echo $show_intro ?>" data-show-automatic-intro="<?php echo $show_automatic_intro ?>">

  <div class="cf-container-logo auto-width">
    <?php if ($show_intro): ?>
      <i id="show-intro" class="glyphicon glyphicon-align-justify pull-right"></i>
    <?php endif; ?>

    <div class="row">
      <div class="overlay col-lg-8 col-xs-8">
        <img src="<?php echo CFieldTheme_Image::get_img_by_id($elements['one']['imageLogo']) ?>"/>

        <div class="overlay-shadow">
          <img src="<?php echo CFieldTheme_Image::get_img_by_id($elements['one']['image']) ?>"/>

          <div class="overlay-content">
            <a href="<?php echo get_permalink($elements['one']['page']) ?>"
               class="btn light"><?php echo $elements['one']['title'] ?></a>
          </div>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="overlay col-lg-4 col-xs-4">
        <img src="<?php echo CFieldTheme_Image::get_img_by_id($elements['second']['imageLogo']) ?>"/>

        <div class="overlay-shadow">
          <img src="<?php echo CFieldTheme_Image::get_img_by_id($elements['second']['image']) ?>"/>

          <div class="overlay-content" style="top: 39%;">
            <a href="<?php echo get_permalink($elements['second']['page']) ?>" class="btn light"
               style="padding: 6px 7px;"><?php echo $elements['second']['title'] ?></a>
          </div>
        </div>
      </div>

      <div id="cf-moving-sun"></div>

      <div class="overlay col-lg-4 col-xs-4">
        <img id="cf-sun" src="<?php echo CFieldTheme_Image::get_img_by_id($elements['third']['imageLogo']) ?>"
             class="cf-M"
             style="display: none;margin-top: 2px;z-index: 2;position: relative;"/>

        <div class="overlay-shadow cf-overlay-shadow-sun"
             style="z-index: 3;top: 1px;width: 104%;left: -3px;">
          <img src="<?php echo CFieldTheme_Image::get_img_by_id($elements['third']['image']) ?>"/>

          <div class="overlay-content" style="top: 39.5%;">
            <a href="<?php echo get_permalink($elements['third']['page']) ?>" class="btn light"
               style="padding: 6px 7px;"><?php echo $elements['third']['title'] ?></a>
          </div>
        </div>
      </div>

      <div class="overlay col-lg-4 col-xs-4">
        <img src="<?php echo CFieldTheme_Image::get_img_by_id($elements['four']['imageLogo']) ?>"
             style="width: 88%;"/>

        <div class="overlay-shadow" style="width: 88%;">
          <img src="<?php echo CFieldTheme_Image::get_img_by_id($elements['four']['image']) ?>"
               style="height: 100%;"/>

          <div class="overlay-content" style="top: 39%;">
            <a href="<?php echo get_permalink($elements['four']['page']) ?>" class="btn light"
               style="padding: 6px 7px;"><?php echo $elements['four']['title'] ?></a>
          </div>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-lg-12 col-xs-12">
        <div class="overlay cf-overlay-blog">
          <div id="cf-textillate" class="cf-font-olympic"
               style="display: none;"><?php echo $elements['fifth']['slogan'] ?>
          </div>

          <div class="overlay-shadow cf-overlay-shadow-sun"
               style="height: 71px;background: rgba(30, 46, 66, 0.8);width: 95.4%;">
            <img src="<?php echo CFieldTheme_Image::get_img_by_id($elements['fifth']['image']) ?>"/>

            <div class="overlay-content" style="top: 18px;">
              <a href="<?php echo get_permalink($elements['fifth']['page']) ?>" class="btn light"
                 style="padding: 6px 7px;"><?php echo $elements['fifth']['title'] ?></a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div id="side-intro" class="side-intro-normal col-md-8 col-xs-12">
    <!--    <i id="close-intro" class="glyphicon glyphicon-remove-circle"></i>-->

    <div class="row">
      <div class="content-intro col-md-12">
        <?php
        // Start the loop.
        while (have_posts()) : the_post();

          // Include the page content template.
          get_template_part('content', 'page');

          // If comments are open or we have at least one comment, load up the comment template.
          if (comments_open() || get_comments_number()) :
            comments_template();
          endif;

          if ($post_meta_array['cftheme_show_author'][0]) :
            get_template_part('author-bio');
          endif;

          // End the loop.
        endwhile;
        ?>
      </div>
    </div>
  </div>
</div>
<!-- /container -->

<div class="row cf-space"></div>

<div class="row">
  <div class="col-md-12">
    <div id="cf-footer" class="cf-footer">
      <!--Cosas del footer aqui-->
    </div>
  </div>
</div>

<script src="<?php echo CFieldTheme::$theme_url . '/bower_components/jquery/dist/jquery.min.js' ?>"></script>
<script src="<?php echo CFieldTheme::$theme_url . '/bower_components/letteringjs/jquery.lettering.js' ?>"></script>
<script src="<?php echo CFieldTheme::$theme_url . '/bower_components/textillate/jquery.textillate.js' ?>"></script>

<script src="<?php echo CFieldTheme::$theme_url . '/app-yeo/scripts/main.js' ?>"></script>
</body>
</html>

