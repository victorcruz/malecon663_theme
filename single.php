<?php
/**
 * The template for displaying all single posts and attachments
 *
 * package     CFieldTheme
 * @subpackage CFieldTheme
 * @since      0.1.0
 */

get_header();

/**
 * Get Style Sidebar (Columns).
 */
$columns_type = ( $post_meta_array['cftheme_column_sidebar'][0] ) ? $post_meta_array['cftheme_column_sidebar'][0] : 'no-sidebar';
$columns_type = json_decode( $columns_type, true );
$right        = ( $columns_type['right'] ) ? $columns_type['right'] : null;
$left         = ( $columns_type['left'] ) ? $columns_type['left'] : null;
$class_center = 'col-md-12';
switch ($columns_type['value']) {
  case "both-sidebar":
    $class_center = 'col-md-6';
    break;
  case "right-sidebar":
    $class_center = 'col-md-9';
    break;
  case "left-sidebar":
    $class_center = 'col-md-9';
    break;
}
?>

<div class="container">
  <div class="row">

    <?php if ($left): ?>
      <aside class="mt50">
        <div class="col-md-3">
          <?php dynamic_sidebar( $left ) ?>
        </div>
      </aside>
    <?php endif ?>

    <section class="blog mt50">
      <div class="<?php echo $class_center ?>">

        <?php
        // Start the loop.
        while (have_posts()) : the_post();

          // Include the page content template.
          get_template_part( 'content', 'post' );

          // If comments are open or we have at least one comment, load up the comment template.
          if (comments_open() || get_comments_number()) :
            comments_template();
          endif;

          // End the loop.
        endwhile;
        ?>

      </div>
    </section>

    <?php if ($right): ?>
      <aside class="mt50">
        <div class="col-md-3">
          <?php dynamic_sidebar( $right ) ?>
        </div>
      </aside>
    <?php endif ?>

  </div>
</div>

<?php get_footer(); ?>
