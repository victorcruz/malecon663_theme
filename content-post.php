<?php
/**
 * The template for displaying all single posts and attachments
 *
 * package     CFieldTheme
 * @subpackage CFieldTheme
 * @since      0.1.0
 */

$post_meta_array                           = get_post_custom( get_the_ID() );
$featured_images                           = json_decode(
  $post_meta_array['cftheme_feaured_multiple_images'][0],
  true
);
$option_show_featured_image_in_single_post = $post_meta_array['cftheme_show_featured_image_in_post'][0];
$option_show_featured_image                = $featured_images['options']['show'];
$option_show_in_post_featured_image        = $featured_images['options']['show_in_post'];
$featured_images                           = $featured_images['images'];
?>

  <!-- Article -->
  <article>
    <!-- Article Image-->
    <?php if ( !$option_show_in_post_featured_image || count( $featured_images ) == 0): ?>
      <?php if (!$option_show_featured_image_in_single_post): ?>
        <?php if (has_post_thumbnail()): ?>
          <a href="<?php echo get_permalink() ?>" class="mask">
            <div style="max-height: 344px;">
              <img src="<?php echo CFieldTheme_Image::get_img_by_id( get_post_thumbnail_id() ) ?>" alt="image"
                   class="img-responsive zoom-img">
            </div>
          </a>
        <?php endif; ?>
      <?php endif; ?>
    <?php endif; ?>

    <!-- Article Slider-->
    <?php if ($option_show_in_post_featured_image && count( $featured_images ) > 0): ?>
      <section class="standard-slider">
        <div class="cf-owl-carousel-featured owl-carousel">
          <?php foreach ($featured_images as $item): ?>
            <?php $image_url = CFieldTheme_Image::get_img_by_id( $item['id'] ); ?>
            <div class="item">
              <a href="<?php echo get_permalink( get_the_ID() ) ?>">
                <img src="<?php echo $image_url ?>" alt="image" class="img-responsive zoom-img">
              </a>
            </div>
          <?php endforeach ?>
        </div>
      </section>
    <?php endif; ?>

    <div class="row">
      <div class="col-sm-1 col-xs-3 col-md-2">
        <?php
        $arr_date   = explode( " ", date_i18n( 'd M' ) );
        $this_day   = $arr_date['0'];
        $this_month = $arr_date['1'];
        ?>
        <div class="meta-date">
          <span><?php echo $this_month; ?></span><?php echo $this_day; ?>
        </div>
      </div>
      <div class="col-sm-11 col-xs-9 col-md-10">
        <h2>
          <a href="<?php echo get_permalink() ?>"><?php the_title() ?></a>
        </h2>

      <span class="meta-author">
        <i class="fa fa-user"></i>
        <a href="<?php echo get_the_author_link() ?>"><?php the_author_meta( 'first_name' ) ?></a>
      </span>
      <span class="meta-category">
        <i class="fa fa-pencil"></i>
        <?php
        $post_categories  = wp_get_post_categories( get_the_ID() );
        $total_categories = count( $post_categories );
        $count            = 1;
        foreach ($post_categories as $c) {
          $cat = get_category( $c );
          ?>
          <a href="<?php echo get_category_link( $cat->cat_ID ) ?>"><?php echo $cat->name ?></a>
          <?php
          if ($count != $total_categories) {
            echo '|';
          }
          ?>
          <?php $count++;
        } ?>
      </span>
      <span class="meta-comments">
        <i class="fa fa-comment"></i>
        <a href="<?php echo get_permalink() ?>">
          <?php echo wp_count_comments()->total_comments ?>
          <?php echo _cftheme_e( 'Comments' ) ?>
        </a>
      </span>
      </div>

      <div class="col-md-12">
        <?php the_content() ?>
      </div>
    </div>
  </article>

  <!-- Blog: Author -->
<?php
if ($post_meta_array['cftheme_show_author'][0]) {
  get_template_part( 'author-bio' );
}
?>