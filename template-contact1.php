<?php
/*
Template Name: Contact 1
*/

/**
 * The template for displaying all single posts and attachments
 *
 * package     CFieldTheme
 * @subpackage CFieldTheme
 * @since      0.1.0
 */

get_header();

$post_meta_array   = get_post_custom( get_the_ID() );
$shortcode_contact = $post_meta_array['cftheme_shortcode_contact'][0];

$featured_image = get_post_thumbnail_id();
if ( $featured_image ) {
  $featured_image = CFieldTheme_Image::get_img_by_id( $featured_image );
} else {
  $featured_image = '';
}

$address = get_theme_mod( 'address_contact' );
?>

<!-- GMap -->
<div id="map-contact1">
  <p>This will be replaced with the Google Map.</p>
</div>

<div class="container">
  <div class="row">

    <!-- Contact form -->
    <section id="contact-form" class="mt50">
      <div class="col-md-8">
        <h2 class="lined-heading"><span><?php echo _cftheme__( 'Send a message' ) ?></span></h2>

        <p>
          <?php
          if ( have_posts() ) : while ( have_posts() ) : the_post();

            the_content();

          endwhile;
          endif;
          ?>
        </p>

        <div class="mt50">
          <?php if ( $shortcode_contact ): ?>
            <?php echo do_shortcode( $shortcode_contact ); ?>
          <?php endif ?>
        </div>

    </section>

    <!-- Contact details -->
    <section class="contact-details mt50">
      <div class="col-md-4">
        <h2 class="lined-heading"><span><?php echo _cftheme__( 'Address' ) ?></span></h2>

        <?php if ( $featured_image ): ?>
          <a href="<?php echo $featured_image ?>" data-rel="prettyPhoto">
            <img src="<?php echo $featured_image ?>"
                 alt="<?php echo $featured_image ?>"
                 class="img-thumbnail img-responsive">
          </a>
        <?php endif ?>

        <address class="mt50">
          <strong><?php bloginfo( 'title' ); ?></strong>
          <br>
          <?php if ( $address ): ?>
            <?php echo $address ?><br>
          <?php endif ?>
          <abbr title="Phone"><?php echo _cftheme__( 'Phone' ) ?>:</abbr> <a
            href="#"><?php echo get_theme_mod( 'phone_number' ) ?></a><br>
          <abbr title="Email"><?php echo _cftheme__( 'Email' ) ?>:</abbr> <a
            href="mailto:<?php echo get_theme_mod( 'email_contact' ); ?>"><?php echo get_theme_mod( 'email_contact' ) ?></a><br>
        </address>

        <h2 class="lined-heading mt50"><span><?php echo _cftheme__( 'Social' ) ?></span></h2>

        <div class="row">
          <?php if ( get_theme_mod( 'facebook' ) ): ?>
            <div class="col-xs-4">
              <div class="box-icon">
                <a href="<?php echo get_theme_mod( 'facebook' ) ?>">
                  <div class="circle square"><i class="fa fa-facebook fa-lg"></i></div>
                </a>
              </div>
            </div>
          <?php endif ?>

          <?php if ( get_theme_mod( 'twitter' ) ): ?>
            <div class="col-xs-4">
              <div class="box-icon">
                <a href="<?php echo get_theme_mod( 'twitter' ) ?>">
                  <div class="circle square"><i class="fa fa-twitter fa-lg"></i></div>
                </a>
              </div>
            </div>
          <?php endif ?>
        </div>

      </div>
    </section>

  </div>
</div>

<style>
  #map-contact1 {
    border-color : #364E51 !important;
  }

  #map-contact1 {
    width         : 100%;
    height        : 300px;
    overflow      : visible !important;
    border        : 0px solid;
    border-bottom : 1px solid;
    margin-top    : -1px;
  }

  #map-contact1 img {
    max-width : none !important;
  }
</style>

<script>
  /**
   * Google Map.
   */
  //Gmap
  if (jQuery().gMap) {
    jQuery('#map-contact1').gMap({
      zoom   : 16, //Integer: Level of zoom in to the map
      markers: [{
        address    : "Malecón 663, Havana, Cuba", //Address of the company
        html       : "<h4><?php bloginfo( 'title' ); ?></h4><p><?php bloginfo( 'description' ); ?></p>",
        popup      : false, //Boolean
        scrollwheel: false, //Boolean
        maptype    : 'TERRAIN', //Choose between: 'HYBRID', 'TERRAIN', 'SATELLITE' or 'ROADMAP'.
        icon       : {
          image     : "<?php echo CFieldTheme::$theme_url.'/images/logo-map.png' ?>",
          iconsize  : [42, 53],
          iconanchor: [12, 46]
        },

        controls: {
          panControl        : false, //Boolean
          zoomControl       : false, //Boolean
          mapTypeControl    : true, //Boolean
          scaleControl      : true, //Boolean
          streetViewControl : true, //Boolean
          overviewMapControl: false //Boolean
        }
      }]
    });
  }
</script>
<?php get_footer(); ?>
