jQuery(function ($) {
  var is_mobile = false;
  if (navigator.userAgent.match(/Android/i) || navigator.userAgent.match(/webOS/i)
      || navigator.userAgent.match(/iPhone/i) //|| navigator.userAgent.match(/iPad/i)
      || navigator.userAgent.match(/iPod/i) || navigator.userAgent.match(/BlackBerry/i)) {
    is_mobile = true;
  }

  var showIntro = $('#cf-container-layout').attr('data-show-intro');
  var showAutomaticIntro = $('#cf-container-layout').attr('data-show-automatic-intro');

  var $sunrise = $('#cf-moving-sun');
  var $sun = $('#cf-sun');
  var $cfTextillate = $('#cf-textillate');
  $('#cf-moving-sun').on('webkitAnimationEnd animationend webkitAnimationStart keyframe', function (e) {
    if (e.type == 'animationend' || e.type == 'webkitAnimationEnd') {
      //$sunrise.hide();
      $sun.show();
      $cfTextillate.show();

      /**
       * Textillate plugin jquery.
       */
        // flash bounce shake tada swing wobble pulse flip flipInX flipOutX flipInY flipOutY
        // fadeIn fadeInUp fadeInDown fadeInLeft fadeInRight fadeInUpBig fadeInDownBig fadeInLeftBig
        // fadeInRightBig fadeOut fadeOutUp fadeOutDown fadeOutLeft fadeOutRight fadeOutUpBig fadeOutDownBig
        // fadeOutLeftBig fadeOutRightBig bounceIn bounceInDown bounceInUp bounceInLeft bounceInRight bounceOut
        // bounceOutDown bounceOutUp bounceOutLeft bounceOutRight rotateIn rotateInDownLeft rotateInDownRight
        // rotateInUpLeft rotateInUpRight rotateOut rotateOutDownLeft rotateOutDownRight rotateOutUpLeft
        // rotateOutUpRight hinge rollIn rollOut
      $cfTextillate.textillate({
        //loop: true,
        in      : {
          effect    : 'flipInX',
          delayScale: 2 // Aqui está la velocidad en que aparecen las letras
          //shuffle   : true
        },
        callback: function () {
          setTimeout(function () {
            guideLogo();
          }, 1000);
        }
      });
    }
  });

  var stateOnLogo = false;
  $('.cf-container-logo').on('mouseover', function () {
    stateOnLogo = true;
  }).on('mouseleave', function () {
    stateOnLogo = false;
  });

  $overlayShadow = $('.overlay-shadow');

  function guideLogo() {
    $overlayShadow.each(function (index) {
      var $this = $(this);

      if (!stateOnLogo) {
        //$this.delay(1000 * index).animate({opacity: 1}, 0, function () {
        //    $this.addClass("cf-guide").delay(1000).queue(function () {
        //        $(this).removeClass("cf-guide").dequeue().animate({opacity: 0}, 500).removeAttr("style");
        //        //$this.removeAttr("style");
        //    });
        //});

        $this.delay(1000 * index).animate({
          duration: 1000
        }, function () {
          $this.addClass("cf-guide")
              .delay(1000)
              .queue(function () {

                $(this).removeClass("cf-guide")
                    .dequeue()
                    .animate({
                      duration: 1000
                    });
              });
        });


        //$this.addClass("cf-guide")
        //    .delay(4500)
        //    .queue(function () {
        //        $(this).removeClass("cf-guide");
        //        $(this).dequeue();
        //    });

        //setInterval(function () {
        //    $this.toggleClass('cf-guide')
        //}, 3000);
      }
    });

    setTimeout(function () {
      if (showAutomaticIntro) {
        splitLayoutOpenOrClose();
      }
    }, 7000);
  }

  //guideLogo();

  $('a').on('click', function () {
    $(this).fadeOut("slow");
    $('.overlay').fadeOut("slow");
  });

  /**
   * ==========================================================================================================
   */

  /**
   * Split Layout.
   */

  var isIpad = navigator.userAgent.match(/iPad/i);

  var inAnimatedContainerLayout = '';
  var outAnimatedContainerLayout = '';

  //inAnimatedContainerLayout = 'animated bounceInRight';
  //outAnimatedContainerLayout = '';

  var $showIntro = $('#show-intro');
  var $containerLayout = $('#cf-container-layout');
  var $containerLogo = $('.cf-container-logo');
  var $sideIntro = $('#side-intro');

  // Open or Close split layout intro.
  function splitLayoutOpenOrClose() {
    if ($containerLayout.hasClass('container')) {
      if (!is_mobile) {
        $containerLayout.removeClass('container animation-close ' + outAnimatedContainerLayout);
        $containerLayout.addClass('row animation ' + inAnimatedContainerLayout);

        $('#cf-moving-sun').hide();
        $containerLogo.removeClass('auto-width');
        if (!isIpad) {
          $containerLogo.addClass('col-md-4 col-xs-12 left-side border-intro');
        } else {
          $sideIntro.removeClass('col-md-8').addClass('col-md-7');
          $containerLogo.addClass('col-md-5 col-xs-12 left-side border-intro');
        }
      } else {
        $sideIntro.removeClass('side-intro-normal').addClass('side-intro-mobile');
        $('body').css('overflow-y', 'auto');
        $('.cf-font-olympic').addClass('cf-font-olympic-mobile');
        $sideIntro.addClass('intro-mobile');
      }

      $showIntro.removeClass('glyphicon-align-justify').addClass('glyphicon-remove-circle');
      $sideIntro.show();
    } else {
      if (!is_mobile) {
        $containerLayout.removeClass('row animation ' + inAnimatedContainerLayout);
        $containerLayout.addClass('container animation-close ' + outAnimatedContainerLayout);

        $('#cf-moving-sun').show();
        $containerLogo.addClass('auto-width');
        if (!isIpad) {
          $containerLogo.removeClass('col-md-4 col-xs-12 left-side border-intro');
        } else {
          $sideIntro.removeClass('col-md-7').addClass('col-md-8');
          $containerLogo.removeClass('col-md-5 col-xs-12 left-side border-intro');
        }
      } else {
        $sideIntro.removeClass('side-intro-mobile').addClass('side-intro-normal');
        $('body').css('overflow-y', 'hidden');
        $('.cf-font-olympic').removeClass('cf-font-olympic-mobile');
        $sideIntro.removeClass('intro-mobile');
      }

      $showIntro.removeClass('glyphicon-remove-circle').addClass('glyphicon-align-justify');
      $sideIntro.hide();
    }
  };

  $('#show-intro').bind('click', function () {
    splitLayoutOpenOrClose();
  });

  $('#close-intro').bind('click', function () {
    splitLayoutOpenOrClose();
  });

  /**
   * ==========================================================================================================
   */

  /**
   * Audio Player.
   */
  var animationInClasses = 'animated flipInX';
  var animationOutClasses = 'animated bounceOutDown';

  var autoplay = $('#cf-box-audio-player').attr('data-autoplay');
  if (autoplay) {
    $('#cf-page-audio-play').hide();
    $('#cf-page-audio-pause').show();
  } else {
    $('#cf-page-audio-play').show();
    $('#cf-page-audio-pause').hide();
  }

  var automaticShowPlayer = $('#cf-box-audio-player').attr('data-automatic_show_player');
  if (automaticShowPlayer) {
    $('#cf-box-audio-player').addClass(animationInClasses).show();
  }

  var automaticTimeCloseMedia = $('#cf-box-audio-player').attr('data-automatic_time_close_media');
  if (automaticTimeCloseMedia && parseInt(automaticTimeCloseMedia) > 0) {
    setTimeout(function () {
      $('.cf-show-audio-player').removeClass(animationInClasses).hide().addClass(animationOutClasses);
    }, parseInt(automaticTimeCloseMedia) * 1000);
  }

  $('#cf-show-audio-player').bind('click', function () {
    // bounceInLeft, bounceInDown, flipInX, rollIn
    $('#cf-box-audio-player').addClass(animationInClasses).show();
  });

  $('#cf-page-audio-play').bind('click', function () {
    var pageAudio = document.getElementById("cf-page-audio");
    pageAudio.play();
    $('#cf-page-audio-play').hide();
    $('#cf-page-audio-pause').show();
  });

  $('#cf-page-audio-pause').bind('click', function () {
    var pageAudio = document.getElementById("cf-page-audio");
    pageAudio.pause();
    $('#cf-page-audio-pause').hide();
    $('#cf-page-audio-play').show();
  });

  $('#cf-box-audio-player i.icon-remove').bind('click', function () {
    $('.cf-show-audio-player').removeClass(animationInClasses).hide().addClass(animationOutClasses);
  });

  /**
   * ==========================================================================================================
   */
});