<?php
/**
 * Malecon663 Theme functions and definitions.
 *
 * @package    Malecon663
 * @subpackage Malecon663 Theme
 * @since      0.1.0
 */

// START SITEURL.
update_option( 'siteurl', 'http://www.malecon663.com' );
update_option( 'home', 'http://www.malecon663.com' );

/**
 * The function for the internationalization.
 *
 * _cftheme_e( '' )
 * _cftheme__( '' )
 */
if ( ! function_exists( '_cftheme_e' ) ) {
  function _cftheme_e( $insStr ) {
    _e( $insStr, 'CFieldTheme' );
  }
}
if ( ! function_exists( '_cftheme__' ) ) {
  function _cftheme__( $insStr ) {
    return __( $insStr, 'CFieldTheme' );
  }
}

if ( ! class_exists( 'CFieldTheme' ) ) {
  /**
   * Class CFieldTheme
   */
  class CFieldTheme{
    /**
     * The Theme version.
     *
     * @const string
     */
    const VERSION = '0.1';

    /**
     * The url to the theme.
     *
     * @static
     * @var string
     */
    static $theme_url;

    /**
     * The path to the theme.
     *
     * @static
     * @var string
     */
    static $theme_dir;

    /**
     * The path to the theme include dir.
     *
     * @static
     * @var string
     */
    static $theme_includes_dir;

    /**
     * The path to the bower theme include dir.
     *
     * @static
     * @var string
     */
    static $theme_bower_url;

    /**
     * The name of the theme.
     *
     * @static
     * @var string
     */
    static $theme_name;

    /**
     * The prefix for the theme internationalization.
     *
     * @static
     * @var string
     */
    static $i18n_prefix;

    /**
     * Executes all initialization code for the theme.
     */
    function __construct() {
      // Define static values.
      self::$theme_url          = get_template_directory_uri();
      self::$theme_dir          = get_template_directory();
      self::$theme_includes_dir = self::$theme_dir . '/includes/';
      self::$theme_bower_url    = self::$theme_url . '/bower_components/malecon663/';
      self::$theme_name         = wp_get_theme()->get( 'Name' );
      self::$i18n_prefix        = 'CFieldTheme';

      add_theme_support( 'post-thumbnails' );
      add_filter( 'upload_mimes', [ &$this, 'wpcontent_svg_mime_type' ] );

      // Makes theme available for translation.
      load_theme_textdomain( self::$i18n_prefix, self::$theme_dir . '/lang' );

      // Include all Classes.
      include( self::$theme_includes_dir . 'classes/all-classes.php' );

      // Include all Breadcrumbs.
      include( self::$theme_includes_dir . 'frontend/breadcrumbs/all-breadcrumbs.php' );

      // Include all Comments.
      include( self::$theme_includes_dir . 'frontend/comments/all-comments.php' );

      // Include all ShortCodes.
      include( self::$theme_includes_dir . 'frontend/shortcodes/all-shortcodes.php' );

      // Include all Widgets.
      include( self::$theme_includes_dir . 'widgets/all-widgets.php' );

      // Include all Menus.
      include( self::$theme_includes_dir . 'menus/all-menus.php' );

      // Operations with Ajax.
      //include( self::$theme_includes_dir . 'ajax.php' );

      // Include Customizer (Options of Theme).
      include( self::$theme_includes_dir . 'admin/customizers/all-customizers.php' );

      // Include Options (Options of Page, Post and Other Type Posts).
      include( self::$theme_includes_dir . 'admin/options/all-options.php' );

      // Enqueues scripts and styles for front end.
      add_action( 'wp_enqueue_scripts', [ &$this, 'load_frontend_assets' ], 0 );

      // Enqueues scripts and styles for admin panel.
      add_action( 'admin_enqueue_scripts', [ &$this, 'load_admin_assets' ], 0 );

      // Add excerpts to pages.
      add_action( 'init', [ &$this, 'add_excerpts_to_pages' ], 0 );
    }

    /**
     * Add excerpts to pages.
     */
    function add_excerpts_to_pages() {
      add_post_type_support( 'page', 'excerpt' );
    }

    /**
     * Enqueues scripts and styles for front end.
     *
     * @since cfieldtheme 0.1.0
     * @return void
     */
    function load_frontend_assets() {
      include( self::$theme_includes_dir . 'frontend/assets/all-assets.php' );
    }

    /**
     * Enqueues scripts and styles for admin panel.
     *
     * @param $hook
     *
     * @since cfieldtheme 0.1.0
     * @return void
     */
    function load_admin_assets( $hook ) {
      include( self::$theme_includes_dir . 'admin/assets/all-assets.php' );
    }

    /**
     * Handles the translation of plugin
     */
    static function handle_load_domain() {
      $locale = self::get_current_lang();

      if ( file_exists( CFieldTheme::$theme_dir . '/lang/' . $locale . '.mo' ) ) {
        load_textdomain( CFieldTheme::$i18n_prefix, CFieldTheme::$theme_dir . '/lang/' . $locale . '.mo' );
      }
    }

    /**
     * Get current language.
     *
     * @return mixed|void
     */
    static function get_current_lang() {
      $lang = explode( '_', get_locale() );

      return $lang[0];
    }

    /**
     * Add SVG capabilities
     */
    function wpcontent_svg_mime_type( $mimes = [ ] ) {
      $mimes['svg']  = 'image/svg+xml';
      $mimes['svgz'] = 'image/svg+xml';

      return $mimes;
    }
  } // End class CFieldTheme.
}

try{
  if ( class_exists( 'CFieldTheme' ) ) {
    new CFieldTheme();
  }
}catch( Exception $e ){
  $exit_msg = 'Problem activating theme: ' . $e->getMessage();
  exit ( $exit_msg );
}
