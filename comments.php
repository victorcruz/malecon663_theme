<?php
/**
 * The template for displaying comments
 *
 * The area of the page that contains both current comments
 * and the comment form.
 *
 * package     CFieldTheme
 * @subpackage CFieldTheme
 * @since      0.1.0
 */

$post_id = get_the_ID();

?>

  <!-- Blog: Comments -->
  <section class="comments mt30">
    <div class="blog-comments">
      <h2 class="lined-heading"><span><i class="fa fa-comments"></i>
          <?php echo get_comments_number() ?>
          <?php echo _cftheme_e( 'Comments' ) ?></span></h2>
    </div>

    <?php if (have_comments()) : ?>
      <?php wp_list_comments(
        [
          'type'     => 'comment',
          'callback' => 'cfieldtheme_display_custom_comments',
        ]
      ); ?>
    <?php endif; // have_comments() ?>

    <?php comment_form(); ?>
  </section>