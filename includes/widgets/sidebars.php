<?php
/**
 * Register Sidebars.
 *
 * @package    CFieldTheme
 * @subpackage CFieldTheme
 * @since      0.1.0
 */

function cfieldtheme_register_sidebars()
{
  // Register Sidebar: Right Column.
  register_sidebar(
    [
      'name'          => _cftheme__( 'Right Column' ),
      'id'            => 'cfieldtheme-right',
      'description'   => '',
      'before_widget' => '<div class="widget ad">',
      'after_widget'  => '</div>',
      'before_title'  => '<strong class="adtitle">',
      'after_title'   => '</strong>'
    ]
  );

  // Register Sidebar: Left Column.
  register_sidebar(
    [
      'name'          => _cftheme__( 'Left Column' ),
      'id'            => 'cfieldtheme-left',
      'description'   => '',
      'before_widget' => '<div class="widget ad">',
      'after_widget'  => '</div>',
      'before_title'  => '<strong class="adtitle">',
      'after_title'   => '</strong>'
    ]
  );

  // Register Sidebar: Footer 1.
  register_sidebar(
    [
      'name'          => _cftheme__( 'Footer 1' ),
      'id'            => 'cfieldtheme-footer1',
      'description'   => '',
      'before_widget' => '<div class="widget ad">',
      'after_widget'  => '</div>',
      'before_title'  => '<strong class="adtitle">',
      'after_title'   => '</strong>'
    ]
  );

  // Register Sidebar: Footer 2.
  register_sidebar(
    [
      'name'          => _cftheme__( 'Footer 2' ),
      'id'            => 'cfieldtheme-footer2',
      'description'   => '',
      'before_widget' => '<div class="widget ad">',
      'after_widget'  => '</div>',
      'before_title'  => '<strong class="adtitle">',
      'after_title'   => '</strong>'
    ]
  );

  // Register Sidebar: Footer 3.
  register_sidebar(
    [
      'name'          => _cftheme__( 'Footer 3' ),
      'id'            => 'cfieldtheme-footer3',
      'description'   => '',
      'before_widget' => '<div class="widget ad">',
      'after_widget'  => '</div>',
      'before_title'  => '<strong class="adtitle">',
      'after_title'   => '</strong>'
    ]
  );

  // Register Sidebar: Footer 4.
  register_sidebar(
    [
      'name'          => _cftheme__( 'Footer 4' ),
      'id'            => 'cfieldtheme-footer4',
      'description'   => '',
      'before_widget' => '<div class="widget ad">',
      'after_widget'  => '</div>',
      'before_title'  => '<strong class="adtitle">',
      'after_title'   => '</strong>'
    ]
  );
}

add_action( 'widgets_init', 'cfieldtheme_register_sidebars', 1 );