<?php
include CFieldTheme::$theme_includes_dir.'widgets/sidebars.php';

$theme_dir = CFieldTheme::$theme_includes_dir.'widgets/classes/';

// Include all options classes.
// Include all .php files into ./includes/widgets/classes folder.
foreach (glob( $theme_dir.'*.php' ) as $filename) {
  include_once( $filename );
}