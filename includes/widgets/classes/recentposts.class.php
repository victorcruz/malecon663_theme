<?php

add_action( 'widgets_init', register_widget( 'CFieldTheme_Widget_Recent_Posts' ), 1 );

/**
 * CFieldTheme extend from WP.
 * Recent_Posts widget class
 *
 * @since 2.8.0
 */
class CFieldTheme_Widget_Recent_Posts extends WP_Widget
{

  public function __construct()
  {
    $widget_ops = [
      'classname'   => 'cfieldtheme_widget_recent_entries',
      'description' => _cftheme__( "Your site&#8217;s most recent Posts." )
    ];
    $this->WP_Widget(
      'cfieldtheme_widget_recent_entries',
      sprintf( _cftheme__( '%s - Recent Posts' ), wp_get_theme()->get( 'Name' ) ),
      $widget_ops
    );

    //    $this->alt_option_name = 'widget_recent_entries';
    //
    //    add_action( 'save_post', [ $this, 'flush_widget_cache' ] );
    //    add_action( 'deleted_post', [ $this, 'flush_widget_cache' ] );
    //    add_action( 'switch_theme', [ $this, 'flush_widget_cache' ] );
  }

  public function widget( $args, $instance )
  {
    $cache = [ ];
    if ( !$this->is_preview()) {
      $cache = wp_cache_get( 'widget_recent_posts', 'widget' );
    }

    if ( !is_array( $cache )) {
      $cache = [ ];
    }

    if ( !isset( $args['widget_id'] )) {
      $args['widget_id'] = $this->id;
    }

    if (isset( $cache[$args['widget_id']] )) {
      echo $cache[$args['widget_id']];

      return;
    }

    ob_start();

    $title = ( !empty( $instance['title'] ) ) ? $instance['title'] : _cftheme__( 'Recent Posts' );

    /** This filter is documented in wp-includes/default-widgets.php */
    $title = apply_filters( 'widget_title', $title, $instance, $this->id_base );

    $number = ( !empty( $instance['number'] ) ) ? absint( $instance['number'] ) : 5;
    if ( !$number) {
      $number = 5;
    }
    $show_date = isset( $instance['show_date'] ) ? $instance['show_date'] : false;

    /**
     * Filter the arguments for the Recent Posts widget.
     *
     * @since 3.4.0
     *
     * @see   WP_Query::get_posts()
     *
     * @param array $args An array of arguments used to retrieve the recent posts.
     */
    $r = new WP_Query(
      apply_filters(
        'widget_posts_args',
        [
          'posts_per_page'      => $number,
          'no_found_rows'       => true,
          'post_status'         => 'publish',
          'ignore_sticky_posts' => true
        ]
      )
    );

    if ($r->have_posts()) :
      ?>
      <?php echo $args['before_widget']; ?>
      <?php if ($title) {
      echo '<h3>'.$title.'</h3>';
    } ?>
      <ul class="list-unstyled">
        <?php while ($r->have_posts()) : $r->the_post(); ?>
          <li>

            <article>
              <div class="news-thumb" style="width: 65px;">
                <?php
                $feat_image = CFieldTheme_Image::get_img_by_id( get_post_thumbnail_id( get_the_ID() ), 'thumbnail' );
                ?>
                <a href="<?php the_permalink(); ?>">
                  <img src="<?php echo $feat_image ? $feat_image : CFieldTheme::$theme_url.'/images/logo.png'; ?>"
                       alt="65x65">
                </a>
              </div>
              <div class="news-content clearfix">
                <h4 style="margin: 10px 0 7px;position: relative;left: 13px;">
                  <a href="<?php the_permalink(); ?>"><?php get_the_title() ? the_title() : the_ID(); ?></a>
                </h4>
                <?php if ( $show_date ) : ?>
                <span style="margin: 9px 0 7px;position: relative;left: 13px;">
                  <a href="<?php the_permalink(); ?>"><?php echo get_the_date(); ?></a>
                </span>
              </div>
              <?php endif; ?>
            </article>

          </li>
        <?php endwhile; ?>
      </ul>
      <?php echo $args['after_widget']; ?>
      <?php
      // Reset the global $the_post as this query will have stomped on it
      wp_reset_postdata();

    endif;

    if ( !$this->is_preview()) {
      $cache[$args['widget_id']] = ob_get_flush();
      wp_cache_set( 'widget_recent_posts', $cache, 'widget' );
    } else {
      ob_end_flush();
    }
  }

  public function update( $new_instance, $old_instance )
  {
    $instance              = $old_instance;
    $instance['title']     = strip_tags( $new_instance['title'] );
    $instance['number']    = (int) $new_instance['number'];
    $instance['show_date'] = isset( $new_instance['show_date'] ) ? (bool) $new_instance['show_date'] : false;
    $this->flush_widget_cache();

    $alloptions = wp_cache_get( 'alloptions', 'options' );
    if (isset( $alloptions['widget_recent_entries'] )) {
      delete_option( 'widget_recent_entries' );
    }

    return $instance;
  }

  public function flush_widget_cache()
  {
    wp_cache_delete( 'widget_recent_posts', 'widget' );
  }

  public function form( $instance )
  {
    $title     = isset( $instance['title'] ) ? esc_attr( $instance['title'] ) : '';
    $number    = isset( $instance['number'] ) ? absint( $instance['number'] ) : 5;
    $show_date = isset( $instance['show_date'] ) ? (bool) $instance['show_date'] : false;
    ?>
    <p><label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label>
      <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>"
             name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo $title; ?>"/></p>

    <p><label for="<?php echo $this->get_field_id( 'number' ); ?>"><?php _e( 'Number of posts to show:' ); ?></label>
      <input id="<?php echo $this->get_field_id( 'number' ); ?>" name="<?php echo $this->get_field_name( 'number' ); ?>"
             type="text" value="<?php echo $number; ?>" size="3"/></p>

    <p><input class="checkbox" type="checkbox" <?php checked( $show_date ); ?>
              id="<?php echo $this->get_field_id( 'show_date' ); ?>"
              name="<?php echo $this->get_field_name( 'show_date' ); ?>"/>
      <label for="<?php echo $this->get_field_id( 'show_date' ); ?>"><?php _e( 'Display post date?' ); ?></label></p>
    <?php
  }
}