<?php

add_action( 'widgets_init', register_widget( 'CFieldTheme_Widget_Categories' ), 1 );

/**
 * CFieldTheme extend from WP.
 * Categories widget class
 *
 * @since 2.8.0
 */
class CFieldTheme_Widget_Categories extends WP_Widget
{

  public function __construct()
  {
    $widget_ops = [
      'classname'   => 'cfieldtheme_widget_categories',
      'description' => _cftheme__( 'A list or dropdown of categories.' )
    ];
    $this->WP_Widget(
      'cfieldtheme_widget_categories',
      sprintf( _cftheme__( '%s - Categories' ), wp_get_theme()->get( 'Name' ) ),
      $widget_ops
    );
  }

  public function widget( $args, $instance )
  {

    /** This filter is documented in wp-includes/default-widgets.php */
    $title = apply_filters(
      'widget_title',
      empty( $instance['title'] ) ? _cftheme__( 'Categories' ) : $instance['title'],
      $instance,
      $this->id_base
    );

    $c = !empty( $instance['count'] ) ? '1' : '0';
    $h = !empty( $instance['hierarchical'] ) ? '1' : '0';
    $d = !empty( $instance['dropdown'] ) ? '1' : '0';

    //echo $args['before_widget'];
    echo '<div class="widget">';
    if ($title) {
      echo '<h3>'.$title.'</h3>';
    }

    $cat_args = [
      'orderby'      => 'name',
      'show_count'   => $c,
      'hierarchical' => $h
    ];

    ?>

    <ul class="list-unstyled">
      <?php
      $cat_args['title_li'] = '';
      $categories           = get_categories( $cat_args );
      ?>

      <?php foreach ($categories as $category) {
        // Get the URL of this category
        $category_link = get_category_link( $category->cat_ID );

        $args = [
          'category'    => $category->cat_ID,
          'numberposts' => -1,
          'post_type'   => [ 'post' ]
        ];

        $posts = get_posts( $args );
        ?>

        <li>
          <a href="<?php echo esc_url( $category_link ); ?>">
            <?php echo $category->name ?> <?php if ($c): ?><span class="badge pull-right"><?php echo count(
              $posts
            ) ?></span><?php endif; ?>
          </a>
        </li>

      <?php } ?>

    </ul>
    <?php

    echo '</div>';
    //echo $args['after_widget'];
  }

  public function update( $new_instance, $old_instance )
  {
    $instance          = $old_instance;
    $instance['title'] = strip_tags( $new_instance['title'] );
    $instance['count'] = !empty( $new_instance['count'] ) ? 1 : 0;
    //$instance['hierarchical'] = !empty( $new_instance['hierarchical'] ) ? 1 : 0;
    //$instance['dropdown']     = !empty( $new_instance['dropdown'] ) ? 1 : 0;

    return $instance;
  }

  public function form( $instance )
  {
    //Defaults
    $instance = wp_parse_args( (array) $instance, [ 'title' => '' ] );
    $title    = esc_attr( $instance['title'] );
    $count    = isset( $instance['count'] ) ? (bool) $instance['count'] : false;
    //$hierarchical = isset( $instance['hierarchical'] ) ? (bool) $instance['hierarchical'] : false;
    //$dropdown     = isset( $instance['dropdown'] ) ? (bool) $instance['dropdown'] : false;
    ?>
    <p><label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label>
      <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>"
             name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo $title; ?>"/></p>

    <!--    <p><input type="checkbox" class="checkbox" id="--><?php //echo $this->get_field_id( 'dropdown' );
    ?><!--"-->
    <!--              name="--><?php //echo $this->get_field_name( 'dropdown' );
    ?><!--"--><?php //checked( $dropdown );
    ?><!-- />-->
    <!--      <label for="--><?php //echo $this->get_field_id( 'dropdown' );
    ?><!--">--><?php //_e( 'Display as dropdown' );
    ?><!--</label><br/>-->

    <input type="checkbox" class="checkbox" id="<?php echo $this->get_field_id( 'count' ); ?>"
           name="<?php echo $this->get_field_name( 'count' ); ?>"<?php checked( $count ); ?> />
    <label for="<?php echo $this->get_field_id( 'count' ); ?>"><?php _e( 'Show post counts' ); ?></label><br/><br/>

    <!--      <input type="checkbox" class="checkbox" id="--><?php //echo $this->get_field_id( 'hierarchical' );
    ?><!--"-->
    <!--             name="--><?php //echo $this->get_field_name( 'hierarchical' );
    ?><!--"--><?php //checked( $hierarchical );
    ?><!-- />-->
    <!--      <label for="--><?php //echo $this->get_field_id( 'hierarchical' );
    ?><!--">--><?php //_e( 'Show hierarchy' );
    ?><!--</label></p>-->
    <?php
  }

}