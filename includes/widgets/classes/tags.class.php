<?php

add_action( 'widgets_init', register_widget( 'CFieldTheme_Widget_Tags' ), 1 );

/**
 * CFieldTheme
 * Tags widget class
 *
 * @since 0.1.0
 */
class CFieldTheme_Widget_Tags extends WP_Widget
{

  public function __construct()
  {
    $widget_ops = [
      'classname'   => 'cfieldtheme_widget_tags',
      'description' => _cftheme__( 'A list or dropdown of categories.' )
    ];
    $this->WP_Widget(
      'cfieldtheme_widget_tags',
      sprintf( _cftheme__( '%s - Tags' ), wp_get_theme()->get( 'Name' ) ),
      $widget_ops
    );
  }

  public function widget( $args, $instance )
  {

    /** This filter is documented in wp-includes/default-widgets.php */
    $title = apply_filters(
      'widget_title',
      empty( $instance['title'] ) ? _cftheme__( 'Tags' ) : $instance['title'],
      $instance,
      $this->id_base
    );

    //echo $args['before_widget'];
    echo '<div class="widget">';
    if ($title) {
      echo '<h3>'.$title.'</h3>';
    }

    $tags = get_tags();

    ?>


    <div class="tags">
      <?php foreach ($tags as $tag) {
        $tag_link = get_tag_link( $tag->term_id );
        ?>

        <a href="<?php echo $tag_link ?>"><?php echo $tag->name ?></a>

      <?php } ?>
    </div>

    <?php

    echo '</div>';
    //echo $args['after_widget'];
  }

  public function update( $new_instance, $old_instance )
  {
    $instance          = $old_instance;
    $instance['title'] = strip_tags( $new_instance['title'] );

    return $instance;
  }

  public function form( $instance )
  {
    //Defaults
    $instance = wp_parse_args( (array) $instance, [ 'title' => '' ] );
    $title    = esc_attr( $instance['title'] );
    ?>
    <p><label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label>
      <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>"
             name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo $title; ?>"/></p>
    <?php
  }

}