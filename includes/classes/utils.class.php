<?php
/**
 * Utils Class.
 *
 * @package    CFieldTheme
 * @subpackage CFieldTheme
 * @since      0.1.0
 */

if (!class_exists('CFieldTheme_Utils')) {

  class CFieldTheme_Utils
  {

    /**
     * Constructor
     */
    function __construct()
    {
    }

    /**
     * Generate token.
     *
     * @return string
     */
    public function generate_random_string()
    {
      return md5(uniqid(rand(), true));
    }

    /**
     * Get url by id.
     *
     * @param        $id
     *
     * @return null
     */
    public function get_url_by_id($id)
    {
      $url = wp_get_attachment_url($id);

      if (isset($url) && !empty($url)) {
        return $url;
      } else {
        return null;
      }
    }
  }
}