<?php
/**
 * Excerpt Class.
 *
 * @package    CFieldTheme
 * @subpackage CFieldTheme
 * @since      0.1.0
 */

if ( ! class_exists( 'CFieldTheme_Excerpt' ) ) {

  class CFieldTheme_Excerpt{
    /**
     * Constructor
     */
    function __construct() {
    }

    public function get_excerpt_by_id( $post_id ) {
      $the_post    = get_post( $post_id );
      $the_excerpt = $the_post->post_excerpt;

      return $the_excerpt;
    }
  }
}