<?php
/**
 * Image Utils Class.
 *
 * @package    CFieldTheme
 * @subpackage CFieldTheme
 * @since      0.1.0
 */

if ( !class_exists( 'CFieldTheme_Image' )) {

  class CFieldTheme_Image
  {

    /**
     * Constructor
     */
    function __construct()
    {
    }

    /**
     * Get images url by id.
     *
     * @param        $image_id
     * @param string $size
     *
     * @return null
     */
    public function get_img_by_id( $image_id, $size = 'full' )
    {
      $image = wp_get_attachment_image_src( $image_id, $size );

      if (isset( $image ) && !empty( $image )) {
        $img_url = $image[0];

        return $img_url;
      } else {
        return null;
      }
    }

  }
}