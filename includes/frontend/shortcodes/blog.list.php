<?php
/**
 * Shortcode
 *
 * package     CFieldTheme
 * @subpackage CFieldTheme
 * @since      0.1.0
 */

/**
 * Shortcode Blog
 *
 * Example:
 * [blog cat="4"]
 *
 * @param $atts
 */
function blog( $atts ) {
  extract( shortcode_atts( [ 'cat' => '' ], $atts ) );

  $cat = "{$cat}";

  $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;

  $args = [
    'cat'       => $cat,
    'post_type' => 'post',
    'paged'     => $paged
  ];

  echo '<section class="blog">';
  query_posts( $args );
  if ( have_posts() ) {
    while ( have_posts() ) {
      the_post();

      /*
       * Include the Post-Format-specific template for the content.
       * If you want to override this in a child theme, then include a file
       * called content-___.php (where ___ is the Post Format name) and that will be used instead.
       */
      get_template_part( 'content', get_post_format() );
    }

    ?>

    <!-- Pagination -->
    <div class="text-center mt50">
      <?php
      // Previous/next page navigation.
      the_posts_pagination( [
        'prev_text'          => _cftheme__( 'Previous page' ),
        'next_text'          => _cftheme__( 'Next page' ),
        'before_page_number' => '<span class="meta-nav screen-reader-text"> </span>',
        'screen_reader_text' => ' ',
      ] );
      ?>
    </div>

    <?php

    echo '</section>';

    //Reset query
    wp_reset_query();
  } else {
    get_template_part( 'content', 'none' );
  }
}

add_shortcode( 'blog', 'blog' );