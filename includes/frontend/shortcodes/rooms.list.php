<?php
/**
 * Shortcode
 *
 * package     CFieldTheme
 * @subpackage CFieldTheme
 * @since      0.1.0
 */

/**
 * Show rooms list.
 *
 * @param $atts
 * @return string
 */
function rooms($atts)
{
  ob_start();
  extract(shortcode_atts(['type' => 'all'], $atts));

  $type = "{$type}";

  $post_meta_array = get_post_custom(get_the_ID());
  $type_header = $post_meta_array['cftheme_type_header'][0];
  $class_type_header = ($type_header == 'cftheme_image_id_header') ? '' : 'mt50';

  $args = [
      'post__not_in'   => [get_the_ID()],
      'posts_per_page' => -1,
      'post_type'      => 'page',
  ];
  $rooms_query = new WP_Query($args);
  ?>

  <?php if ($type == 'all'): ?>
  <!-- Alojamiento -->
  <div class="container <?php echo $class_type_header ?>">
    <div class="col-sm-12">
      <h2 class="lined-heading bounceInLeft appear" data-start="0">
        <span><?php echo _cftheme__('Rooms') ?></span>
      </h2>
    </div>

    <?php if ($rooms_query->have_posts()): ?>
      <?php while ($rooms_query->have_posts()) : $rooms_query->the_post(); ?>
        <?php

        $item_meta_array = get_post_custom(get_the_ID());
        $is_room = $item_meta_array['cftheme_this_page_is_room'][0];
        ?>

        <?php if ($is_room == 'cftheme_room_options'): ?>
          <?php
          $featured_images = json_decode($post_meta_array['cftheme_feaured_multiple_images'][0],
              true);
          $option_show_featured_image = $featured_images['options']['show'];
          $option_show_in_post_featured_image = $featured_images['options']['show_in_post'];
          $featured_images = $featured_images['images'];

          $price = $item_meta_array['cftheme_price'][0];
          //$price = ($price) ? $price : '--';

          $services = json_decode($item_meta_array['cftheme_room_options'][0],
              true);
          ?>

          <div class="row cf-room-list lined-heading pulse appear">
            <!-- Slider -->
            <section class="standard-slider room-slider">
              <div class="col-sm-12 col-md-8">

                <?php if ($option_show_in_post_featured_image && count($featured_images) > 0): ?>
                  <div class="cf-owl-carousel-rooms owl-standard owl-carousel">
                    <?php foreach ($featured_images as $item): ?>
                      <?php $image_url = CFieldTheme_Image::get_img_by_id($item['id']); ?>
                      <div class="item">
                        <a href="<?php echo get_permalink(get_the_ID()) ?>"
                           data-rel="prettyPhoto[gallery1]">
                          <img src="<?php echo $image_url ?>" alt=""
                               class="img-responsive"></a>
                      </div>
                    <?php endforeach ?>
                  </div>
                <?php endif ?>

                <?php if (!$option_show_in_post_featured_image || count($featured_images) == 0): ?>
                  <?php if (has_post_thumbnail()): ?>
                    <div class="cf-owl-carousel-rooms owl-standard owl-carousel">
                      <a href="<?php echo get_permalink() ?>" class="">
                        <img src="<?php echo CFieldTheme_Image::get_img_by_id(get_post_thumbnail_id()) ?>"
                             alt=""
                             class="img-responsive zoom-img">
                      </a>
                    </div>
                  <?php endif; ?>
                <?php endif ?>

              </div>
            </section>

            <!-- Reservation form -->
            <section class="clearfix">
              <div class="col-sm-12 col-md-4">
                <div class="room-thumb cf-room-alojamiento">
                  <div class="mask">
                    <div class="main">
                      <h5><?php the_title() ?></h5>

                      <?php if ($price): ?>
                        <div class="price">$ <?php echo $price ?><span><?php _cftheme_e('the night') ?></span></div>
                      <?php endif; ?>
                    </div>
                    <div class="content">
                      <p><?php echo substr(CFieldTheme_Excerpt::get_excerpt_by_id($page->ID), 0, 150) ?></p>

                      <div class="row" style="max-height: 86px;overflow: auto;">
                        <?php if (count($services) > 0): ?>
                          <?php foreach ($services as $item): ?>
                            <div class="col-xs-6">
                              <ul class="list-unstyled">
                                <li><i class="fa fa-check-circle"></i> <?php echo $item['name'] ?></li>
                              </ul>
                            </div>
                          <?php endforeach ?>
                        <?php endif ?>

                        <?php if (count($services) == 0): ?>
                          <div class="alert alert-warning alert-dismissable" style="font-size: 12px;">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <?php echo _cftheme__('The services of this room have not been specified') ?>
                          </div>
                        <?php endif ?>
                      </div>
                      <a href="<?php echo get_permalink() ?>"
                         class="btn btn-primary btn-block"><?php _cftheme_e('Read More') ?></a>
                    </div>
                  </div>
                </div>
              </div>
            </section>
          </div>
        <?php endif ?>
      <?php endwhile ?>
    <?php endif;
    wp_reset_postdata(); ?>
  </div>
<?php endif ?>

  <?php return ob_get_clean();
}

add_shortcode('rooms', 'rooms');