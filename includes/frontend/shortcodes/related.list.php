<?php
/**
 * Shortcode
 *
 * package     CFieldTheme
 * @subpackage CFieldTheme
 * @since      0.1.0
 */

add_shortcode('related', 'related');
/**
 * Show rooms list.
 *
 * @param $atts
 *
 * Params:
 * - type: 'short', 'all'
 *
 * @return string
 */
function related($atts)
{
  ob_start();
  extract(shortcode_atts(['type' => 'all'], $atts));

  $type = "{$type}";

  $post_meta_array = get_post_custom(get_the_ID());
  $type_header = $post_meta_array['cftheme_type_header'][0];
  $class_type_header = ($type_header == 'cftheme_image_id_header') ? '' : 'mt50';

  //  $args          = [
  //    'post__not_in'   => [ get_the_ID() ],
  //    'posts_per_page' => - 1,
  //    'post_type'      => 'page',
  //  ];
  //  $related_query = new WP_Query( $args );

  $args = [
      'exclude'     => get_the_ID(),
      'post_type'   => 'page',
      'post_status' => 'publish'
  ];

  $pages = get_pages($args);
  ?>

  <!-- Show other rooms --------------------------------------------------------------------------------------------- -->

  <section class="rooms">
    <div class="container">
      <div class="row">
        <div class="col-sm-12">
          <h2 class="lined-heading bounceInLeft appear" data-start="0">
            <span><?php echo _cftheme__('Rooms') ?></span>
          </h2>
        </div>

        <?php foreach ($pages as $page): ?>
          <?php

          $item_meta_array = get_post_custom($page->ID);
          $is_room = $item_meta_array['cftheme_this_page_is_room'][0];

          $content = CFieldTheme_Excerpt::get_excerpt_by_id($page->ID);
          //$content = apply_filters( 'the_content', $content );
          ?>

          <?php if ($is_room == 'cftheme_room_options'): ?>
            <?php
            $price = $item_meta_array['cftheme_price'][0];
            //$price = ( $price ) ? $price : '--';

            $services = json_decode($item_meta_array['cftheme_room_options'][0], true);
            ?>

            <!-- Room -->
            <div class="col-sm-4">
            <div class="room-thumb">
            <?php if (has_post_thumbnail($page->ID)): ?>
              <?php echo get_the_post_thumbnail($page->ID, 'full', ['class' => 'img-responsive', 'alt' => get_the_title($page->ID)]) ?>
            <?php endif ?>

          <div class="mask">
            <div class="main">
              <h5><?php echo get_the_title($page->ID) ?></h5>

              <?php if ($price): ?>
                <div class="price">$ <?php echo $price ?><span><?php _cftheme_e('the night') ?></span></div>
              <?php endif; ?>
            </div>

            <div class="content">
              <p><?php echo substr($content, 0, 145) ?> ...</p>

              <div class="row" style="max-height: 105px;overflow: auto;">
                <?php if (count($services) > 0): ?>
                  <?php foreach ($services as $item): ?>
                    <div class="col-xs-6">
                      <ul class="list-unstyled">
                        <li><i class="fa fa-check-circle"></i> <?php echo $item['name'] ?></li>
                      </ul>
                    </div>
                  <?php endforeach ?>
                <?php endif ?>

                <?php if (count($services) == 0): ?>
                  <div class="alert alert-warning alert-dismissable" style="font-size: 12px;">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo _cftheme__('The services of this room have not been specified') ?>
                  </div>
                <?php endif ?>
              </div>

              <a href="<?php echo get_permalink($page->ID) ?>"
                 class="btn btn-primary btn-block"><?php _cftheme_e('Read More') ?></a>
            </div>
          </div>
          </div>
          </div>
          <?php endif ?>
        <?php endforeach ?>
      </div>
    </div>
  </section>

  <?php return ob_get_clean();
}