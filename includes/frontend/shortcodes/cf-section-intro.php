<?php
/**
 * Shortcode
 *
 * package     CFieldTheme
 * @subpackage CFieldTheme
 * @since      0.1.0
 */

/**
 * Show Intro Box 1.
 *
 * @param $atts
 * @param null $content
 * @return string
 */
function intro_box1($atts, $content = null)
{
  ob_start();
  extract(shortcode_atts(['title' => '', 'class' => '', 'color' => ''], $atts));

  $title = "{$title}";
  $class = "{$class}";
  $color = "{$color}";
  ?>

  <section class="cf-section-intro <?php echo $class ?> bg-secondary pb0"
           style="background-color: <?php echo $color ?>;">
    <div class="">
      <div class="row">
        <div class="col-sm-12 text-center">
          <h2 class="mb64 cf-title"><?php echo $title ?></h2>
        </div>
        <div class="col-md-8 col-md-offset-2 col-sm-12 text-center">
          <?php echo $content; ?>
        </div>
      </div>
    </div>
  </section>

  <?php return ob_get_clean();
}

add_shortcode('intro1', 'intro_box1');
