<?php
/**
 * Shortcode
 *
 * package     CFieldTheme
 * @subpackage CFieldTheme
 * @since      0.1.0
 */

add_shortcode( 'categories', 'categories' );
/**
 * Show categories list.
 *
 * @param $atts
 *
 * Params:
 * - type: 'short', 'all'
 *
 * @return string
 */
function categories( $atts ) {
  ob_start();
  extract( shortcode_atts( [ 'parent' => '' ], $atts ) );
  $parent = "{$parent}";

  $categories = get_categories( [ 'parent' => $parent ] );
  //var_dump( $categories );

  $args = [
    'post_type'      => 'post',
    'posts_per_page' => - 1,
    'category'       => $parent
  ];

  $posts_array = get_posts( $args );
  ?>

  <div class="" style="margin-top: -24px;">
    <div class="row">
      <div class="col-sm-12">
        <ul class="nav nav-pills" id="filters">
          <li class="active"><a href="#" data-filter="*"><?php _cftheme_e( 'ALL' ) ?></a></li>

          <?php foreach ( $categories as $category ): ?>
            <li>
              <a href="#" data-filter=".<?php echo $category->slug ?>"><?php echo $category->name ?></a>
            </li>
          <?php endforeach ?>
        </ul>
      </div>
    </div>
  </div>

  <!-- Gallery -->
  <section id="gallery" class="mt50">
    <div class="container">
      <div class="row gallery">

        <?php foreach ( $posts_array as $post_item ): ?>
          <?php
          $post_categories = wp_get_post_categories( $post_item->ID );
          $cats            = '';

          foreach ( $post_categories as $c ) {
            $cat = get_category( $c );
            $cats .= $cat->slug . ' ';
          }


          ?>

          <div class="col-sm-3 <?php echo $cats ?> fadeIn appear">
            <a href="<?php echo CFieldTheme_Image::get_img_by_id( get_post_thumbnail_id( $post_item->ID ) ) ?>"
               data-rel="prettyPhoto[gallery1]">
              <img
                src="<?php echo CFieldTheme_Image::get_img_by_id( get_post_thumbnail_id( $post_item->ID ) ) ?>"
                alt="image"
                class="img-responsive zoom-img"/>
              <i class="fa fa-search"></i>
            </a>
          </div>

        <?php endforeach ?>

      </div>
    </div>
  </section>

  <?php return ob_get_clean();
}