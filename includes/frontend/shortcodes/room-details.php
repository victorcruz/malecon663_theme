<?php
/**
 * Shortcode
 *
 * package     CFieldTheme
 * @subpackage CFieldTheme
 * @since      0.1.0
 */

/**
 * Show rooms details.
 *
 * @param $atts
 * @return string
 */
function rooms_details($atts)
{
  ob_start();
  extract(shortcode_atts([], $atts));

  $post_meta_array = get_post_custom(get_the_ID());

  $services = json_decode($post_meta_array['cftheme_room_options'][0], true);
  ?>

  <!-- Room Content -->
  <section style="margin-bottom: 35px;">
    <div class="container">
      <div class="row">
        <div class="col-sm-12 mt30">
          <h2 class="lined-heading" style="margin-bottom: 38px;"><span><?php _cftheme_e('Room Details') ?></span></h2>

          <?php if ( count( $services ) > 0 ): ?>
            <?php foreach ( $services as $item ): ?>
              <div class="col-xs-4">
                <ul class="list-unstyled">
                  <li><i class="fa fa-check-circle"></i> <?php echo $item['name'] ?></li>
                </ul>
              </div>
            <?php endforeach ?>
          <?php endif ?>
        </div>
      </div>
    </div>
  </section>

  <?php return ob_get_clean();
}

add_shortcode('rooms-details', 'rooms_details');