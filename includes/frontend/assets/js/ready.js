jQuery(function ($) {
  var is_mobile = false;
  if (navigator.userAgent.match(/Android/i) || navigator.userAgent.match(/webOS/i)
    || navigator.userAgent.match(/iPhone/i) || navigator.userAgent.match(/iPad/i)
    || navigator.userAgent.match(/iPod/i) || navigator.userAgent.match(/BlackBerry/i)) {
    is_mobile = true;
  }

  /**
   * ==========================================================================================================
   */

  /**
   * Audio Player.
   */
  var animationInClasses = 'animated flipInX';
  var animationOutClasses = 'animated bounceOutDown';

  var autoplay = $('#cf-box-audio-player').attr('data-autoplay');
  if (autoplay) {
    $('#cf-page-audio-play').hide();
    $('#cf-page-audio-pause').show();
  } else {
    $('#cf-page-audio-play').show();
    $('#cf-page-audio-pause').hide();
  }

  var automaticShowPlayer = $('#cf-box-audio-player').attr('data-automatic_show_player');
  if (automaticShowPlayer) {
    $('#cf-box-audio-player').addClass(animationInClasses).show();
  }

  var automaticTimeCloseMedia = $('#cf-box-audio-player').attr('data-automatic_time_close_media');
  if (automaticTimeCloseMedia && parseInt(automaticTimeCloseMedia) > 0) {
    setTimeout(function () {
      $('.cf-show-audio-player').removeClass(animationInClasses).hide().addClass(animationOutClasses);
    }, parseInt(automaticTimeCloseMedia) * 1000);
  }

  $('#cf-show-audio-player').bind('click', function () {
    // bounceInLeft, bounceInDown, flipInX, rollIn
    $('#cf-box-audio-player').addClass(animationInClasses).show();
  });

  $('#cf-page-audio-play').bind('click', function () {
    var pageAudio = document.getElementById("cf-page-audio");
    pageAudio.play();
    $('#cf-page-audio-play').hide();
    $('#cf-page-audio-pause').show();
  });

  $('#cf-page-audio-pause').bind('click', function () {
    var pageAudio = document.getElementById("cf-page-audio");
    pageAudio.pause();
    $('#cf-page-audio-pause').hide();
    $('#cf-page-audio-play').show();
  });

  $('#cf-box-audio-player i.icon-remove').bind('click', function () {
    $('.cf-show-audio-player').removeClass(animationInClasses).hide().addClass(animationOutClasses);
  });

  /**
   * ==========================================================================================================
   */

  /**
   * Check if options animation slogan is true and animated text.
   *
   * @type {*|jQuery}
   */
  var $sloganAnimate = $('.slogan-textillate').attr('data-slogan-animate');
  if ($sloganAnimate) {
    $('.slogan-textillate').textillate({
      //loop: true,
      in      : {
        effect    : 'flipInX',
        delayScale: 2 // Aqui está la velocidad en que aparecen las letras
        //shuffle   : true
      },
      callback: function () {

      }
    });
  }

  /**
   * Remove extra styles from wordpress.
   */
  $('#wp-admin-css').attr('href', '');
  $('#buttons-css').attr('href', '');
  $('#bogo-css').attr('href', '');

  /**
   * Top Menu.
   */
    // Show dropdown menu on hover.
  $('#main-menu .dropdown').hover(
    function () {
      $('.dropdown-menu', this).fadeIn();
    },
    function () {
      $('.dropdown-menu', this).fadeOut('fast');
    });

  /**
   * Sticky Navigation
   */
  if ($().sticky) {
    $(".navbar").sticky({
      topSpacing: 0,
    });
  }

  var $isHeaderSlider = $('#cf-scroll-header').html();
  if ($isHeaderSlider) {
    var shrinkHeader = 100;
    $(window).scroll(function () {
      var scroll = getCurrentScroll();
      if (scroll >= shrinkHeader) {
        $('.navbar').addClass('shrink');
        $('#main-menu .navbar').addClass('exshrink navbar-default');
        $('#main-menu .navbar-header, #main-menu .logo-box').addClass('exshrink');
      } else {
        $('.navbar').removeClass('shrink');
        $('#main-menu .navbar').removeClass('exshrink navbar-default');
        $('#main-menu .navbar-header, #main-menu .logo-box').removeClass('exshrink');
      }
    });
  }

  function getCurrentScroll() {
    return window.pageYOffset || document.documentElement.scrollTop;
  }

  /**
   * Top Menu Class.
   */
  $('ul[id^="menu-top-menu"]').addClass('nav navbar-nav');

  $('#cf-replace-language').replaceWith(jQuery('.current-lang').text());

  /**
   * Parallax box.
   */
    //$('#parallax-pagetitle').parallax("20%", -0.55);
  $('#parallax-pagetitle').parallax("20%", -1.7);

  /**
   * Slider Featured Single Post.
   */
  jQuery(".cf-owl-carousel-featured").owlCarousel({
    navigation           : true,
    // Show next and prev buttons
    slideSpeed           : 300,
    paginationSpeed      : 400,
    singleItem           : true,
    pagination           : true,
    navigationText       : ['<i class="fa fa-angle-left fa-3x"></i>', '<i class="fa fa-angle-right fa-3x"></i>'],
    responsive           : true,
    responsiveRefreshRate: 200,
    responsiveBaseWidth  : window,
    // "singleItem:true" is a shortcut for:
    // items : 1,
    // itemsDesktop : false,
    // itemsDesktopSmall : false,
    // itemsTablet: false,
    // itemsMobile : false
  });

  /**
   * Slider Featured List Rooms
   */
  jQuery(".cf-owl-carousel-rooms").owlCarousel({
    navigation           : false,
    // Show next and prev buttons
    slideSpeed           : 300,
    paginationSpeed      : 400,
    singleItem           : true,
    pagination           : true,
    navigationText       : ['<i class="fa fa-angle-left fa-3x"></i>', '<i class="fa fa-angle-right fa-3x"></i>'],
    responsive           : true,
    responsiveRefreshRate: 200,
    responsiveBaseWidth  : window,
    // "singleItem:true" is a shortcut for:
    // items : 1,
    // itemsDesktop : false,
    // itemsDesktopSmall : false,
    // itemsTablet: false,
    // itemsMobile : false
  });

  /**
   * Remove to SpireBuilder RichText.
   */
  $('.spirebuilder-widget-rich-text-break').remove();
});