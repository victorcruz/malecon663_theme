<?php
/**
 * Includes needed for the needed front end functionality.
 *
 * @since   0.1.0
 */

/**
 * Styles.
 */

$commons_deps = ['twitter-bootstrap'];

wp_enqueue_style('animate', CFieldTheme::$theme_bower_url . 'css/animate.css');
wp_enqueue_style('bootstrap', CFieldTheme::$theme_bower_url . 'css/bootstrap.min.css', $commons_deps);
wp_enqueue_style('bootstrap-extra', CFieldTheme::$theme_bower_url . 'css/bootstrap.min.css');
wp_enqueue_style('font-awesome-malecon663', CFieldTheme::$theme_bower_url . 'css/font-awesome.min.css', ['font-awesome']);
wp_enqueue_style('owl.carousel', CFieldTheme::$theme_bower_url . 'css/owl.carousel.css');
wp_enqueue_style('owl.theme', CFieldTheme::$theme_bower_url . 'css/owl.theme.css');
wp_enqueue_style('prettyPhoto', CFieldTheme::$theme_bower_url . 'css/prettyPhoto.css');
wp_enqueue_style('jquery-ui-custom',
    CFieldTheme::$theme_bower_url . 'css/smoothness/jquery-ui-1.10.4.custom.min.css');
wp_enqueue_style('theme', CFieldTheme::$theme_bower_url . 'css/theme.css');
wp_enqueue_style('colors', CFieldTheme::$theme_bower_url . 'css/colors/turquoise.css');
wp_enqueue_style('responsive', CFieldTheme::$theme_bower_url . 'css/responsive.css');
wp_enqueue_style('styles', CFieldTheme::$theme_bower_url . 'css/styles.css');
wp_enqueue_style('custom_styles', CFieldTheme::$theme_url . '/includes/frontend/assets/css/theme.css');

/**
 * JavaScript.
 */

wp_enqueue_script("jquery");
wp_enqueue_script('bootstrap', CFieldTheme::$theme_bower_url . 'js/bootstrap.min.js');
wp_enqueue_script('bootstrap-hover-dropdown', CFieldTheme::$theme_bower_url . 'js/bootstrap-hover-dropdown.min.js');
wp_enqueue_script('owl.carousel', CFieldTheme::$theme_bower_url . 'js/owl.carousel.min.js');
wp_enqueue_script('jquery.parallax', CFieldTheme::$theme_bower_url . 'js/jquery.parallax-1.1.3.js');
wp_enqueue_script('jquery.nicescroll', CFieldTheme::$theme_bower_url . 'js/jquery.nicescroll.js');
wp_enqueue_script('jquery.prettyPhoto', CFieldTheme::$theme_bower_url . 'js/jquery.prettyPhoto.js');
wp_enqueue_script('jquery-ui.custom', CFieldTheme::$theme_bower_url . 'js/jquery-ui-1.10.4.custom.min.js');
//wp_enqueue_script( 'jquery.jigowatt', CFieldTheme::$theme_bower_url.'js/jquery.jigowatt.js' );
wp_enqueue_script('jquery.sticky', CFieldTheme::$theme_bower_url . 'js/jquery.sticky.js');
wp_enqueue_script('waypoints', CFieldTheme::$theme_bower_url . 'js/waypoints.min.js');
wp_enqueue_script('jquery.isotope', CFieldTheme::$theme_bower_url . 'js/jquery.isotope.min.js');
wp_enqueue_script('jquery.gmap', CFieldTheme::$theme_bower_url . 'js/jquery.gmap.min.js');
wp_enqueue_script('jquery.gmap.build', 'http://maps.google.com/maps/api/js?sensor=false');
wp_enqueue_script('jquery.lettering', CFieldTheme::$theme_bower_url . 'js/textillate/assets/jquery.lettering.js');
wp_enqueue_script('jquery.textillate', CFieldTheme::$theme_bower_url . 'js/textillate/jquery.textillate.js');
wp_enqueue_script('custom', CFieldTheme::$theme_bower_url . 'js/custom.js');
//wp_enqueue_script( 'main', CFieldTheme::$theme_bower_url.'js/main.js' );
wp_enqueue_script('ready', CFieldTheme::$theme_url . '/includes/frontend/assets/js/ready.js');
