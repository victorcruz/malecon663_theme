<?php

/**
 * Breadcrumbs Default Function.
 * Take from:
 * http://www.codecheese.com/2013/11/wordpress-comment-form-with-twitter-bootstrap-3-supports/
 *
 * @package    CFieldTheme
 * @subpackage CFieldTheme
 * @since      0.1.0
 */

add_filter( 'comment_form_default_fields', 'cfieldtheme_bootstrap3_comment_form_fields' );
function cfieldtheme_bootstrap3_comment_form_fields( $fields )
{
  $commenter = wp_get_current_commenter();
  $req       = get_option( 'require_name_email' );
  $aria_req  = ( $req ? " aria-required='true'" : '' );
  $html5     = current_theme_supports( 'html5', 'comment-form' ) ? 1 : 0;
  $fields    = [
    'author' => '<div class="form-group comment-form-author">'.'<label for="author">'._cftheme__(
        'Name'
      ).( $req ? ' <span class="required">*</span>' : '' ).'</label> '.'<input class="form-control" id="author" name="author" type="text" value="'.esc_attr(
                  $commenter['comment_author']
                ).'" size="30"'.$aria_req.' /></div>',
    'email'  => '<div class="form-group comment-form-email"><label for="email">'._cftheme__(
        'Email'
      ).( $req ? ' <span class="required">*</span>' : '' ).'</label> '.'<input class="form-control" id="email" name="email" '.( $html5 ? 'type="email"' : 'type="text"' ).' value="'.esc_attr(
                  $commenter['comment_author_email']
                ).'" size="30"'.$aria_req.' /></div>',
    'url'    => '<div class="form-group comment-form-url"><label for="url">'._cftheme__( 'Website' ).'</label> '.'<input class="form-control" id="url" name="url" '.( $html5 ? 'type="url"' : 'type="text"' ).' value="'.esc_attr(
                  $commenter['comment_author_url']
                ).'" size="30" /></div>'
  ];

  return $fields;
}

add_filter( 'comment_form_defaults', 'cfieldtheme_bootstrap3_comment_form' );
function cfieldtheme_bootstrap3_comment_form( $args )
{
  $args['comment_field'] = '<div class="form-group comment-form-comment">
    <label for="comment">'._x( 'Comment', 'noun' ).'</label>
    <textarea class="form-control" id="comment" name="comment" cols="45" rows="8" aria-required="true"></textarea>
    </div>';
  $args['class_submit']  = 'btn btn-default btn-lg'; // since WP 4.1
  return $args;
}

//add_action('comment_form', 'cfieldtheme_bootstrap3_comment_button' );
//function cfieldtheme_bootstrap3_comment_button() {
//  echo '<button class="btn btn-default" type="submit">' . _cftheme__( 'Submit' ) . '</button>';
//}