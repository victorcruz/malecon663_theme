<?php

/**
 * Display Comments Default Function.
 * Take from:
 * http://www.paulund.co.uk/customizing-wordpress-comments
 *
 * @package    CFieldTheme
 * @subpackage CFieldTheme
 * @since      0.1.0
 */

/**
 * Display all comments.
 *
 * @param $comment
 * @param $args
 * @param $depth
 */
function cfieldtheme_display_custom_comments( $comment, $args, $depth )
{
  $isByAuthor = false;

  if ($comment->comment_author_email == get_the_author_meta( 'email' )) {
    $isByAuthor = true;
  }

  $GLOBALS['comment'] = $comment; ?>

  <div id="li-comment-<?php comment_ID() ?>" class="comment" style="background-color: rgba(52, 62, 63, 0.05);">

    <?php comment_reply_link(
      array_merge(
        $args,
        [
          'before'     => '<div class="reply-button">',
          'after'      => '</div>',
          'reply_text' => _cftheme__( 'Reply' ),
          'depth'      => $depth,
          'max_depth'  => $args['max_depth']
        ]
      )
    ) ?>

    <div class="avatar">
      <!--      <img src="images/blog/50x50.gif" alt="50x50.gif" class="img-circle"/>-->
    </div>
    <div class="comment-text">
      <div class="author">
        <div class="name">
          <?php echo get_comment_author() ?>
        </div>
        <div class="date">
          <?php echo get_comment_date( 'd M y' ) ?>
          <?php _cftheme_e( 'at' ) ?>
          <?php echo get_comment_date( 'g:s a' ) ?>
        </div>
      </div>

      <div class="text">
        <?php comment_text() ?>
      </div>
    </div>
  </div>
  <?php
}