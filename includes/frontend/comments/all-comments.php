<?php
$theme_dir = CFieldTheme::$theme_includes_dir.'frontend/comments/';

// Include all options classes.
// Include all .php files into ./includes/comments folder.
foreach (glob( $theme_dir.'*.php' ) as $filename) {
  include_once( $filename );
}