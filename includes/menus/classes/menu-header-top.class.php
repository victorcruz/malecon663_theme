<?php

/**
 * Custom WP_NAV_MENU function for top navigation.
 */

if ( !class_exists( 'CFieldTheme_Header_Top_Nav_Menu' )) {

  class CFieldTheme_Header_Top_Nav_Menu extends Walker_Nav_Menu
  {

    // add classes to ul sub-menus
    function display_element( $element, &$children_elements, $max_depth, $depth = 0, $args, &$output )
    {
      $id_field = $this->db_fields['id'];
      if (is_object( $args[0] )) {
        $args[0]->has_children = !empty( $children_elements[$element->$id_field] );
      }

      return parent::display_element( $element, $children_elements, $max_depth, $depth, $args, $output );
    }

    function start_lvl( &$output, $depth )
    {

      $indent  = str_repeat( "\t", $depth );
      $out_div = ( $depth == 0 ? '' : '' );

      // build html
      if ($depth == 0) {
        $output .= "\n".$indent.$out_div.'<ul class="dropdown-menu yamm-content">'."\n";
      } else {
        $output .= "\n".$indent.$out_div.'<ul class="unstyled">'."\n";
      }
    }

    function end_lvl( &$output, $depth = 0, $args = [ ] )
    {
      $indent = str_repeat( "\t", $depth );
      //$out_close = ( $depth == 0 ? '<b class="caret"></b>' : '' );
      $output .= "$indent</ul></li>"."\n";
    }

    // add main/sub classes to li's and links
    function start_el( &$output, $item, $depth, $args )
    {
      $sub    = "";
      $indent = ( $depth > 0 ? str_repeat( "\t", $depth ) : '' ); // code indent
      if ($depth == 0 && $args->has_children) :
        $sub = ' has_sub';
      endif;
      if ($depth == 1 && $args->has_children) :
        $sub = 'sub';
      endif;

      $active = "";

      // passed classes
      $classes = empty( $item->classes ) ? [ ] : (array) $item->classes;

      $class_names = esc_attr( implode( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item ) ) );

      $current_top_li = '';
      if ($item->current) {
        $current_top_li .= ' active';
      }

      // build html
      if ($depth == 0) {

        $output .= $indent.'<li id="nav-menu-item-'.$item->ID.'" class="'.$class_names.' '.$current_top_li.' '.$active.$sub.' dropdown">';
      } elseif ($depth == 1) {

        $title = $item->title;

        $output .= $indent.'<li id="nav-menu-item-'.$item->ID.'" class="'.$class_names.' '.$current_top_li.' '.$active.$sub.' span2">
        <a href="'.$item->url.'">'.$title.'</a>';
      } else {
        $output .= $indent.'<li id="nav-menu-item-'.$item->ID.'" class="'.$class_names.' '.$active.$sub.'">';
      }

      $href = '';
      if ($depth == 0) {
        $href = '#';
        if ( !$args->has_children && !empty( $item->url )) {
          $href = esc_attr( $item->url );
        }
      } elseif ( !empty( $item->url )) {
        $href = esc_attr( $item->url );
      }

      $current_a = "";
      // link attributes
      $attributes = !empty( $item->attr_title ) ? ' title="'.esc_attr( $item->attr_title ).'"' : '';
      $attributes .= !empty( $item->target ) ? ' target="'.esc_attr( $item->target ).'"' : '';
      $attributes .= !empty( $item->xfn ) ? ' rel="'.esc_attr( $item->xfn ).'"' : '';
      $attributes .= ' href="'.$item->url.'" data-url="'.$item->url.'"';

      //$attributes .= !empty( $item->url ) ? ' href="' . esc_attr( $item->url ) . '"' : '';

      if ($item->current):
        $current_a .= ''; // Set css class if is current
      endif;

      if ($depth == 0) {

        $arrow_child = '';
        if ($args->has_children) {
          $attributes .= ' class="'.$current_a.' dropdown-toggle menu-a"';
          $attributes .= ' ';
          $arrow_child = '<b class="caret"></b>';
        } else {
          $attributes .= ' class="'.$current_a.' menu-a"';
        }
      }

      //$attributes .= ' class="' . $current_a . '"';
      $item_output = '';

      if ($depth == 0 || $depth > 1) {
        $item_output = sprintf(
          '%1$s<a%2$s>%3$s%4$s%5$s' . $arrow_child . '</a>%6$s',
          $args->before,
          $attributes,
          $args->link_before,
          ( $args->has_children && $depth != 0 ) ? apply_filters(
            'the_title',
            $item->title,
            $item->ID
          ) : apply_filters( 'the_title', $item->title, $item->ID ),
          $args->link_after,
          $args->after
        );
      }

      // build html
      $output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
    }

    /**
     * @see Walker::end_el()
     *
     * @param string $output Passed by reference. Used to append additional content.
     *
     * @return void
     */
    function end_el( &$output )
    {
      //$output .= '</li>';
      //$output .= '</ul>';
    }

  }
}
