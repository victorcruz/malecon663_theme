<?php
//include CFieldTheme::$theme_includes_dir.'menus/nav-menus.php';

$theme_dir = CFieldTheme::$theme_includes_dir.'menus/classes/';

// Include all options classes.
// Include all .php files into ./includes/menus/classes folder.
foreach (glob( $theme_dir.'*.php' ) as $filename) {
  include_once( $filename );
}

/**
 * Register Menus.
 */
function cfieldtheme_register_menus()
{
  register_nav_menus(
    [
      'header-menu-top'      => _cftheme__( 'Header Menu Top' ),
      'header-menu-floating' => _cftheme__( 'Header Menu Floating' ),
      //'footer-nav'       => _cftheme__( 'Footer Menu' )
    ]
  );
}

add_action( 'init', 'cfieldtheme_register_menus' );