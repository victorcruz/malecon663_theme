<?php
/**
 * Includes needed for the needed admin assets functionality.
 *
 * @since   0.1
 */
global $post;

/**
 * General Styles.
 */

// Include bootstrap css with modification to admin page of wordpress by this tutorial:
// https://stackoverflow.com/questions/23858529/wrapp-twitter-bootstrap-in-a-custom-wordpress-plugin
// Class css to used is: cf-bootstrap-wrapper
wp_enqueue_style(
  'cfieldtheme-admin-bootstrap',
  CFieldTheme::$theme_url.'/includes/admin/assets/css/wordpress-bootstrap.css',
  false
);

wp_enqueue_style(
  'cfieldtheme-admin',
  CFieldTheme::$theme_url.'/includes/admin/assets/css/admin.css',
  false
);

/**
 * General JavaScripts.
 */

wp_enqueue_script( 'bootstrap', CFieldTheme::$theme_url.'/bower_components/bootstrap/dist/js/bootstrap.min.js' );
wp_enqueue_script( 'angularjs', CFieldTheme::$theme_url.'/bower_components/angular/angular.min.js' );

// Load needed styles.
if ($hook == 'post-new.php' || $hook == 'post.php') {
  /**
   * Styles.
   */

  /**
   * JavaScript.
   */
}

if ($hook == 'widgets.php') {
  /**
   * Styles.
   */

  /**
   * JavaScript.
   */
}
