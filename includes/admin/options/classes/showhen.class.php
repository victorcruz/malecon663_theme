<?php
/**
 * Show When click in other Component.
 *
 * @package    CFieldTheme
 * @subpackage CFieldTheme
 * @since      0.1.0
 */

if ( !class_exists( 'CFieldTheme_Option_showhen' )) {
  class CFieldTheme_Option_showhen
  {
    public $type = 'showhen';

    public function render_content( $value, $option )
    {
      ?>

      <div class="row">
        <div class="col-md-12">
          <p class="help-block"><?php echo $option['description']; ?></p>
        </div>
      </div>

      <script>
        jQuery(document).ready(function ($) {
          var key = '<?php echo $option['key'] ?>';
          var $key = $('#' + key);

          var element = '<?php echo $option['id'] ?>';
          var $element = $('#' + element);

          var select = '<?php echo $option['select'] ?>';
          var show = '<?php echo $option['show'] ?>';
          var others = show.split(',');

          function showen() {
            if ($element.attr('value') == select) {
              for (var i = 0; i < others.length; i++) {
                //$('#' + others[i]).show();
                $('#' + others[i]).show().animate({
                  "border-color": "#F5B4B4",
                }, 0).animate({
                  "border-color": "#DDD",
                }, 5000);
              }
              $key.show().animate({
                "border-color": "#F5B4B4",
              }, 0).animate({
                "border-color": "#DDD",
              }, 5000);
            } else {
              for (var i = 0; i < others.length; i++) {
                $('#' + others[i]).hide();
              }
              $key.hide();
            }
          }

          showen();

          $element.bind('click', function (e) {
            showen();
          });

        });
      </script>

      <?php
    }
  }
}
