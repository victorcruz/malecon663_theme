<?php
/**
 * Select show/hide other options components.
 *
 * @package    CFieldTheme
 * @subpackage CFieldTheme
 * @since      0.1.0
 */

if ( !class_exists( 'CFieldTheme_Option_selectoption' )) {
  class CFieldTheme_Option_selectoption
  {
    public $type = 'selectoption';

    public function render_content( $value, $option )
    {
      $token = md5( uniqid( rand(), true ) );
      ?>

      <div class="form-group">
        <label><?php echo $option['title']; ?></label>
        <select id="typeheader_<?php echo $token ?>" name="<?php echo $option['key']; ?>" class="form-control input-sm">
          <?php foreach ($option['options'] as $item): ?>
            <option value="<?php echo $item['value'] ?>"
                    <?php if ( isset( $value ) && $value == $item['value'] ): ?>selected="selected"<?php endif ?>
                    data-others="<?php echo $item['others'] ?>">
              <?php echo $item['name'] ?>
            </option>
          <?php endforeach ?>
        </select>

        <p class="help-block"><?php echo $option['description']; ?></p>
      </div>

      <script>
        jQuery(document).ready(function ($) {
          var initValue = '<?php echo $value ?>';
          var $select = $('#typeheader_<?php echo $token ?>');
          var $options = $('#typeheader_<?php echo $token ?> option');

          function managerTypes(value) {
            $options.each(function () {
              var $this = $(this);
              var idComponent = $this.attr('value');
              if (idComponent != value) {
                $('#' + idComponent).hide();

                var others = $this.attr('data-others');
                if (others) {
                  others = others.split(',');
                  for (var i = 0; i < others.length; i++) {
                    $('#' + others[i]).hide();
                  }
                }
              } else {
                $('#' + idComponent).show().animate({
                  "border-color": "#F5B4B4",
                }, 0).animate({
                  "border-color": "#DDD",
                }, 5000);

                var others = $this.attr('data-others');
                if (others) {
                  others = others.split(',');
                  for (var i = 0; i < others.length; i++) {
                    $('#' + others[i]).show().animate({
                      "border-color": "#F5B4B4",
                    }, 0).animate({
                      "border-color": "#DDD",
                    }, 5000);
                  }
                }
              }
            });

            if (!value || value == '') {
              var firstComponentId = $options.eq(0).attr('value');
              $('#' + firstComponentId).show().animate({
                "border-color": "#F5B4B4",
              }, 0).animate({
                "border-color": "#DDD",
              }, 5000);
            }
          };

          managerTypes(initValue);

          $select.bind('click', function (e) {
            var $this = jQuery(this);
            var selectValue = $this.attr('value');

            managerTypes(selectValue);
          });

        });
      </script>

      <style>
        .cf-box-instanciate {
          border : 3px solid #F5B4B4 !important;
        }
      </style>
      <?php
    }
  }
}