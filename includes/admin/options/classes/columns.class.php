<?php
/**
 * Columns Component.
 *
 * @package    CFieldTheme
 * @subpackage CFieldTheme
 * @since      0.1.0
 */

/**
 * Example use:
 *
 */

if ( !class_exists( 'CFieldTheme_Option_columns' )) {
  class CFieldTheme_Option_columns
  {
    public $type = 'columns';

    public function render_content( $value, $option )
    {
      $default = '';
      ?>
      <div class="form-group">
        <label><?php echo $option['title']; ?></label>

        <div id="<?php echo $option['key']; ?>"> <!-- $elements -->
          <textarea name="<?php echo $option['key']; ?>" style="display: none;"><?php echo $value; ?></textarea>

          <?php foreach ($option['options'] as $item): ?>
            <?php
            if (isset( $item['default'] ) && !$value) {
              $default = 'cf-select';
            }
            ?>
            <img class="cf-to-select <?php echo $default; ?>"
                 src="<?php echo $item['image']; ?>"
                 data-column="<?php echo $item['value']; ?>"/>
            <?php $default = ''; ?>
          <?php endforeach; ?>

          <div class="cf-sidebars right-sidebar" style="display: none;">
            <label><?php echo _cftheme__( 'Right Sidebar' ); ?></label>
            <select class="form-control input-sm">
              <?php foreach ($GLOBALS['wp_registered_sidebars'] as $sidebar) { ?>
                <option value="<?php echo ucwords( $sidebar['id'] ); ?>">
                  <?php echo ucwords( $sidebar['name'] ); ?>
                </option>
              <?php } ?>
            </select>
          </div>

          <div class="cf-sidebars left-sidebar" style="display: none;">
            <label><?php echo _cftheme__( 'Left Sidebar' ); ?></label>
            <select class="form-control input-sm">
              <?php foreach ($GLOBALS['wp_registered_sidebars'] as $sidebar) { ?>
                <option value="<?php echo ucwords( $sidebar['id'] ); ?>">
                  <?php echo ucwords( $sidebar['name'] ); ?>
                </option>
              <?php } ?>
            </select>
          </div>

        </div>
      </div>

      <script>
        var $elements = jQuery('#<?php echo $option['key']; ?>');
        var $columSelect = $elements.find('img');
        var $textarea = $elements.find('textarea');

        var $sidebars = $elements.find('.cf-sidebars');
        var $rightSidebar = $elements.find('.right-sidebar');
        var $leftSidebar = $elements.find('.left-sidebar');

        var newValue = {
          value: 'no-sidebar'
        };

        var originalValue = $textarea.html(); //$input.attr('value');
        if (!originalValue) {
          originalValue = '{"value":"no-sidebar"}';
          $textarea.html(JSON.stringify(newValue));
        }

        var valueObject = JSON.parse(originalValue);
        if (!!valueObject.value) {
          $elements.find('.cf-to-select').each(function () {
            var $thisImg = jQuery(this);
            if ($thisImg.attr('data-column') == valueObject.value) {
              $thisImg.addClass('cf-select');
            }
          });
        }

        if (!!valueObject.value && valueObject.value != 'no-sidebar') {
          newValue.value = valueObject.value;

          if (valueObject.value == 'right-sidebar') {
            $rightSidebar.show();
            $rightSidebar.find('select').val(valueObject.right);
          }

          if (valueObject.value == 'left-sidebar') {
            $leftSidebar.show();
            $leftSidebar.find('select').val(valueObject.left);
          }

          if (valueObject.value == 'both-sidebar') {
            $rightSidebar.show();
            $rightSidebar.find('select').val(valueObject.right);

            $leftSidebar.show();
            $leftSidebar.find('select').val(valueObject.left);
          }
        }

        // Click on image.
        $columSelect.bind('click', function (e) {
          var $this = jQuery(this);
          var value = $this.attr('data-column');

          $sidebars.hide();

          $columSelect.removeClass('cf-select');
          newValue = {
            value: value
          };

          // Show sidebars select.
          if (value == 'right-sidebar') {
            $rightSidebar.show();
            newValue.right = $rightSidebar.find('select').val();
          }

          if (value == 'left-sidebar') {
            $leftSidebar.show();
            newValue.left = $leftSidebar.find('select').val();
          }

          if (value == 'both-sidebar') {
            $rightSidebar.show();
            $leftSidebar.show();

            newValue.right = $rightSidebar.find('select').val();
            newValue.left = $leftSidebar.find('select').val();
          }

          $textarea.html(JSON.stringify(newValue));
          $this.addClass('cf-select');

          e.stopPropagation();
        });

        // Click on select.
        $rightSidebar.find('select').bind('click', function (e) {
          var $thisOption = jQuery(this);

          newValue.right = $thisOption.val();
          $textarea.html(JSON.stringify(newValue));

          e.stopPropagation();
        });

        $leftSidebar.find('select').bind('click', function (e) {
          var $thisOption = jQuery(this);

          newValue.left = $thisOption.val();
          $textarea.html(JSON.stringify(newValue));

          e.stopPropagation();
        });
      </script>

      <style>
        .cf-to-select {
          margin-right : 8px;
        }

        .cf-select {
          border : 1px solid #73AECD !important;
        }

        .cf-sidebars {
          margin-top : 8px;
        }
      </style>
      <?php
    }
  }
}