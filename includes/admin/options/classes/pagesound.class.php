<?php
/**
 * Page Sound Component.
 *
 * @package    CFieldTheme
 * @subpackage CFieldTheme
 * @since      0.1.0
 */

if (!class_exists('CFieldTheme_Option_pagesound')) {
  class CFieldTheme_Option_pagesound
  {
    public $type = 'pagesound';

    public function render_content($value, $option)
    {
      $token = md5(uniqid(rand(), true));

      if ($value) {
        $valueObject = json_decode($value, true);
//        $list        = $valueObject['images'];
//        foreach ($list as $key => $item) {
//          $image_url                          = CFieldTheme_Image::get_img_by_id( $item['id'] );
//          $valueObject['images'][$key]['url'] = $image_url;
//        }
        $value = json_encode($valueObject);
      }
      ?>

      <div ng-controller="pageSoundController as self">

        <div class="row">
          <div class="col-xs-12 col-md-12">
            <label><?php echo $option['title']; ?></label>
            <br/>

            <span ng-show="self.value.sound.id" class="label label-default">
              {{ self.value.sound.title }} ({{ self.value.sound.filename }})
              <br/><br/>
            </span>
          </div>

          <div class="col-md-3">
            <input type="button" value="<?php _cftheme_e('Upload Track'); ?>" ng-click="self.uploadTrack()"
                   class="btn btn-default btn-block btn-sm"/>

            <br/>
            <input type="button" value="<?php _cftheme_e('Upload Cover'); ?>" ng-click="self.upload()"
                   class="btn btn-default btn-block btn-sm"/>

            <img ng-src="{{ self.value.image.url }}" class="img-thumbnail" ng-show="self.value.image.url"/>
            <img src="<?php echo CFieldTheme::$theme_url . '/images/no-image.svg' ?>" class="img-thumbnail"
                 ng-show="!self.value.image.url"/>
          </div>

          <div class="col-md-9" style="margin-top: -13px;">
            <div role="form">
              <div class="checkbox">
                <label>
                  <input type="checkbox" ng-model="self.value.play_sound"> <?php _cftheme_e('Play Sound?'); ?>
                </label>
              </div>

              <div class="checkbox">
                <label>
                  <input type="checkbox" ng-model="self.value.options.autoplay"> <?php _cftheme_e('Autoplay?'); ?>
                </label>
              </div>

              <div class="checkbox">
                <label>
                  <input type="checkbox"
                         ng-model="self.value.options.automatic_show_player"> <?php _cftheme_e('Automatic Show Player?'); ?>
                </label>
              </div>

              <div class="form-group">
                <label><?php _cftheme_e('Automatic close media player after:'); ?></label>

                <div>
                  <input type="number" class="form-control" ng-model="self.value.options.automatic_time_close_media"
                         style="width: 16%;display: inline;">
                  Seconds
                </div>
              </div>
              <div class="form-group">
                <label><?php _cftheme_e('Author or Group name'); ?></label>
                <input type="text" class="form-control" ng-model="self.value.options.author_name">
              </div>
              <div class="form-group">
                <label><?php _cftheme_e('Title'); ?></label>
                <textarea class="form-control" rows="3" ng-model="self.value.options.title"></textarea>
              </div>
            </div>
          </div>

          <div class="col-xs-12 col-md-12">
            <span class="pull-right"
                  title="<?php _cftheme_e('Show json code') ?>"
                  aria-hidden="true"
                  ng-click="self.showJson = true"
                  style="cursor: pointer;"><></span>

              <textarea ng-show="self.showJson"
                        ng-init="self.showJson = false"
                        name="<?php echo $option['key']; ?>"
                        class="form-control"
                        rows="3">{{ self.value }}
              </textarea>

            <p class="help-block"><?php echo $option['description']; ?></p>
          </div>
        </div>

      </div>

      <!-- ------------------------------------------- -->

      <script>
        angular.module("builderOptionsApp")
            .controller('pageSoundController', ['$scope', function ($scope) {
              var $self = this;

              var value = '<?php echo $value; ?>';
              if (value) {
                $self.value = angular.fromJson(value);
              } else {
                $self.value = {
                  play_sound: false,
                  options   : {
                    autoplay                  : false,
                    automatic_show_player     : false,
                    automatic_time_close_media: 0,
                    author_name               : '',
                    title                     : ''
                  },
                  image     : {
                    id : '',
                    url: ''
                  },
                  sound     : {
                    id             : '',
                    url            : '',
                    title          : '',
                    uploadedToTitle: '',
                    filename       : ''
                  }
                };
              }

              $self.upload = function () {
                var custom_uploader = wp.media({
                  title   : '<?php _cftheme_e( 'Upload Image - Page Sound' ); ?>',
                  button  : {
                    text: '<?php _cftheme_e( 'Insert' ); ?>'
                  },
                  multiple: false,
                  library : {type: 'image'}
                })
                    .on('select', function () {
                      var attachment = custom_uploader.state().get('selection').first().toJSON();

                      $self.value.image = {
                        id : attachment.id,
                        url: attachment.url
                      };

                      if (!$scope.$$phase) {
                        $scope.$apply();
                      }
                    })
                    .open();
              };

              $self.uploadTrack = function () {
                var custom_uploader = wp.media({
                  title   : '<?php _cftheme_e( 'Upload Track Sound - Page Sound' ); ?>',
                  button  : {
                    text: '<?php _cftheme_e( 'Insert' ); ?>'
                  },
                  multiple: false,
                  //library : {type: 'image'}
                })
                    .on('select', function () {
                      var attachment = custom_uploader.state().get('selection').first().toJSON();

                      console.log(attachment)

                      $self.value.sound = {
                        id             : attachment.id,
                        url            : attachment.url,
                        title          : attachment.title,
                        uploadedToTitle: attachment.uploadedToTitle,
                        filename       : attachment.filename
                      };

                      if (!$scope.$$phase) {
                        $scope.$apply();
                      }
                    })
                    .open();
              };
            }])
      </script>

      <?php
    }
  }
}