<?php
/**
 * Yeoman Index Data Component.
 *
 * @package    CFieldTheme
 * @subpackage CFieldTheme
 * @since      0.1.0
 */

if ( !class_exists( 'CFieldTheme_Option_yeoindex' )) {
  class CFieldTheme_Option_yeoindex
  {
    public $type = 'yeoindex';

    public function render_content( $value, $option )
    {
      ?>

      <div class="row" ng-controller="yeoController as self">

        <div class="col-md-12">
          <label><?php echo $option['title']; ?></label>
          <br/>
        </div>

        <div class="col-md-2">
          <span class="label label-primary"><?php _cftheme_e( 'First Element (MALE)' ) ?></span>

          <div class="form-group">
            <label><?php _cftheme_e( 'Text' ) ?></label>
            <input type="text" class="form-control" ng-model="self.index.one.title">
          </div>
          <div class="form-group">
            <label><?php _cftheme_e( 'ID Page Redirect' ) ?></label>
            <input type="text" class="form-control" ng-model="self.index.one.page">
          </div>
          <div class="form-group">
            <label><?php _cftheme_e( 'ID Image Logo Piece' ) ?></label>
            <input type="text" class="form-control" ng-model="self.index.one.imageLogo">
          </div>
          <div class="form-group">
            <label><?php _cftheme_e( 'ID Image' ) ?></label>
            <input type="text" class="form-control" ng-model="self.index.one.image">
          </div>
        </div>

        <div class="col-md-2">
          <span class="label label-primary"><?php _cftheme_e( 'Second Element (C)' ) ?></span>

          <div class="form-group">
            <label><?php _cftheme_e( 'Text' ) ?></label>
            <input type="text" class="form-control" ng-model="self.index.second.title">
          </div>
          <div class="form-group">
            <label><?php _cftheme_e( 'ID Page Redirect' ) ?></label>
            <input type="text" class="form-control" ng-model="self.index.second.page">
          </div>
          <div class="form-group">
            <label><?php _cftheme_e( 'ID Image Logo Piece' ) ?></label>
            <input type="text" class="form-control" ng-model="self.index.second.imageLogo">
          </div>
          <div class="form-group">
            <label><?php _cftheme_e( 'ID Image' ) ?></label>
            <input type="text" class="form-control" ng-model="self.index.second.image">
          </div>
        </div>

        <div class="col-md-2">
          <span class="label label-primary"><?php _cftheme_e( 'Third Element (O)' ) ?></span>

          <div class="form-group">
            <label><?php _cftheme_e( 'Text' ) ?></label>
            <input type="text" class="form-control" ng-model="self.index.third.title">
          </div>
          <div class="form-group">
            <label><?php _cftheme_e( 'ID Page Redirect' ) ?></label>
            <input type="text" class="form-control" ng-model="self.index.third.page">
          </div>
          <div class="form-group">
            <label><?php _cftheme_e( 'ID Image Logo Piece' ) ?></label>
            <input type="text" class="form-control" ng-model="self.index.third.imageLogo">
          </div>
          <div class="form-group">
            <label><?php _cftheme_e( 'ID Image' ) ?></label>
            <input type="text" class="form-control" ng-model="self.index.third.image">
          </div>
        </div>

        <div class="col-md-2">
          <span class="label label-primary"><?php _cftheme_e( 'Four Element (N)' ) ?></span>

          <div class="form-group">
            <label><?php _cftheme_e( 'Text' ) ?></label>
            <input type="text" class="form-control" ng-model="self.index.four.title">
          </div>
          <div class="form-group">
            <label><?php _cftheme_e( 'ID Page Redirect' ) ?></label>
            <input type="text" class="form-control" ng-model="self.index.four.page">
          </div>
          <div class="form-group">
            <label><?php _cftheme_e( 'ID Image Logo Piece' ) ?></label>
            <input type="text" class="form-control" ng-model="self.index.four.imageLogo">
          </div>
          <div class="form-group">
            <label><?php _cftheme_e( 'ID Image' ) ?></label>
            <input type="text" class="form-control" ng-model="self.index.four.image">
          </div>
        </div>

        <div class="col-md-2">
          <span class="label label-primary"><?php _cftheme_e( 'Fifth Element (La Combinación Perfecta)' ) ?></span>

          <div class="form-group">
            <label><?php _cftheme_e( 'Text' ) ?></label>
            <input type="text" class="form-control" ng-model="self.index.fifth.title">
          </div>
          <div class="form-group">
            <label><?php _cftheme_e( 'ID Page Redirect' ) ?></label>
            <input type="text" class="form-control" ng-model="self.index.fifth.page">
          </div>
          <div class="form-group">
            <label><?php _cftheme_e( 'ID Image Logo Piece' ) ?></label>
            <input type="text" class="form-control" ng-model="self.index.fifth.imageLogo">
          </div>
          <div class="form-group">
            <label><?php _cftheme_e( 'ID Image' ) ?></label>
            <input type="text" class="form-control" ng-model="self.index.fifth.image">
          </div>
          <div class="form-group">
            <label><?php _cftheme_e( 'Slogan' ) ?></label>
            <input type="text" class="form-control" ng-model="self.index.fifth.slogan">
          </div>
        </div>

        <div class="col-md-12">
          <span class="pull-right"
                title="<?php _cftheme_e( 'Show json code' ) ?>"
                aria-hidden="true"
                ng-click="self.showJson = true"
                style="cursor: pointer;"><></span>
            <textarea ng-show="self.showJson"
                      ng-init="self.showJson = false"
                      name="<?php echo $option['key']; ?>"
                      class="form-control"
                      rows="3"
                      style="display: block;">{{ self.index }}
          </textarea>
        </div>
      </div>

      <!-- ------------------------------------------- -->

      <script>
        angular.module("builderOptionsApp")
          .controller('yeoController', [function () {
            var $self = this;

            var value = '<?php echo $value; ?>';
            if (value) {
              $self.index = angular.fromJson(value);
            } else {
              $self.index = {};
            }

          }]);
      </script>

    <?php }
  }
}
