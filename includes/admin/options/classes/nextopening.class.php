<?php
/**
 * Next Opening Options Component.
 *
 * @package    CFieldTheme
 * @subpackage CFieldTheme
 * @since      0.1.0
 */

if (!class_exists('CFieldTheme_Option_nextopening')) {
  class CFieldTheme_Option_nextopening
  {
    public $type = 'nextopening';

    public function render_content($value, $option)
    {
      $token = md5(uniqid(rand(), true));
      ?>

      <div class="row" ng-controller="nextOpeningController as self">

        <div class="col-md-12">
          <label><?php echo $option['title']; ?></label>
          <br/>

          <div role="form">
            <div class="checkbox">
              <label>
                <input type="checkbox" ng-model="self.nextOpening.show"> <?php _cftheme_e('Show Box') ?>
              </label>
            </div>
            <div class="form-group">
              <label><?php _cftheme_e('Text to show') ?></label>
              <textarea class="form-control" rows="3" ng-model="self.nextOpening.text"></textarea>
            </div>
          </div>

          <span class="pull-right"
                title="<?php _cftheme_e('Show json code') ?>"
                aria-hidden="true"
                ng-click="self.showJson = true"
                style="cursor: pointer;"><></span>
          <textarea ng-show="self.showJson"
                    ng-init="self.showJson = false"
                    name="<?php echo $option['key']; ?>"
                    class="form-control"
                    rows="3"
                    style="display: block;">{{ self.nextOpening }}
        </textarea>
        </div>
      </div>

      <!-- ------------------------------------------- -->

      <script>
        angular.module("builderOptionsApp")
            .controller('nextOpeningController', [function () {
              var $self = this;

              var value = '<?php echo esc_attr( $value ); ?>';
              if (value) {
                $self.nextOpening = angular.fromJson(value);
              } else {
                $self.nextOpening = {
                  show: false,
                  text: ''
                };
              }

            }])
      </script>

      <?php
    }
  }
}