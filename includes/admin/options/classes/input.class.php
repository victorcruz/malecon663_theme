<?php
/**
 * Input Text Component.
 *
 * @package    CFieldTheme
 * @subpackage CFieldTheme
 * @since      0.1.0
 */

/**
 * Example use:
 * [
 * 'title'       => _cftheme__( 'Testing Input Text' ),
 * 'key'         => 'cftheme_inputtext_test',
 * 'type_input'  => 'text', // text, checkbox
 * 'class'       => 'CFieldTheme_Option_input',
 * 'description' => _cftheme__( 'This is a description' )
 * ]
 */

if (!class_exists('CFieldTheme_Option_input')) {
  class CFieldTheme_Option_input
  {
    public $type = 'input';

    public function render_content($value, $option)
    {
      ?>

      <div class="form-group">
        <label><?php echo $option['title']; ?></label>

        <!-- Input Text -->
        <?php if ($option['type_input'] == 'text'): ?>
          <input type="text" class="form-control input-sm"
                 name="<?php echo $option['key']; ?>"
                 value="<?php echo esc_attr($value); ?>"
                 placeholder="<?php echo $option['title']; ?>">

          <p class="help-block"><?php echo $option['description']; ?></p>
        <?php endif ?>

        <!-- Input Text -->
        <?php if ($option['type_input'] == 'checkbox'): ?>
          <div class="checkbox">
            <label>
              <input type="checkbox"
                     name="<?php echo $option['key']; ?>"
                  <?php if ($value): ?> checked<?php endif ?>
              > <?php echo $option['description']; ?>
            </label>
          </div>
        <?php endif ?>

        <!-- Input Text -->
        <?php if ($option['type_input'] == 'textarea'): ?>
          <div class="col-md-12">
            <label><?php _cftheme_e('Text to show') ?></label>
            <textarea class="form-control" rows="3"
                      name="<?php echo $option['key']; ?>"><?php echo esc_attr($value); ?></textarea>
            <?php echo $option['description']; ?>
          </div>
        <?php endif ?>
      </div>

      <?php
    }
  }
}