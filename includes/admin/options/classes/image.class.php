<?php
/**
 * Select Image Component.
 *
 * @package    CFieldTheme
 * @subpackage CFieldTheme
 * @since      0.1.0
 */

if ( !class_exists( 'CFieldTheme_Option_selectimage' )) {
  class CFieldTheme_Option_selectimage
  {
    public $type = 'selectimage';

    public function render_content( $value, $option )
    {
      $token = md5( uniqid( rand(), true ) );
      if ($value) {
        $img_url = CFieldTheme_Image::get_img_by_id( $value );
      }
      ?>

      <div class="form-group">

        <div class="row">
          <div class="col-xs-12 col-md-12">
            <label><?php echo $option['title']; ?></label>
            <br/>
            <input id="cf_button_<?php echo $token ?>" type="button" value="<?php _cftheme_e( 'Upload Image' ); ?>"
                   class="btn btn-default btn-sm"/>
            <input id="cf_button_remove_<?php echo $token ?>" type="button"
                   value="<?php _cftheme_e( 'Remove Image' ); ?>"
                   class="btn btn-danger btn-sm pull-right"/>
            <input id="cf_hidden_<?php echo $token ?>" name="<?php echo $option['key']; ?>" type="hidden"
                   value="<?php echo esc_attr( $value ) ?>"/>
          </div>

          <div class="col-xs-12 col-md-12">
            <br/><br/>

            <div class="thumbnail"
                 style="margin-top: 5px;padding: 0px;border: 0px solid #DDD;margin-bottom: 3px;max-height: 176px;overflow: hidden;border-radius: 0px;">
              <img id="cf_img_<?php echo $token; ?>" class="custom_media_image" src="<?php echo $img_url ?>"/>
            </div>
            <p class="help-block"><?php echo $option['description']; ?></p>
          </div>
        </div>
      </div>

      <script>
        jQuery(document).ready(function () {
          function media_upload(button_class) {
            jQuery(button_class).on('click', function (e) {
              var custom_uploader = wp.media({
                title   : '<?php _cftheme_e( 'Select a Header Image' ); ?>',
                button  : {
                  text: '<?php _cftheme_e( 'Insert' ); ?>'
                },
                multiple: false,
                library : {type: 'image'}
              })
                .on('select', function () {
                  var attachment = custom_uploader.state().get('selection').first().toJSON();
                  jQuery('#cf_hidden_<?php echo $token ?>').val(attachment.id);
                  jQuery('#cf_img_<?php echo $token; ?>').attr('src', attachment.url);
                })
                .open();
            });

            jQuery('#cf_button_remove_<?php echo $token ?>').on('click', function (e) {
              jQuery('#cf_img_<?php echo $token; ?>').attr('src', '');
              jQuery('#cf_hidden_<?php echo $token ?>').val('');
            });
          }

          media_upload('#cf_button_<?php echo $token; ?>');
        });
      </script>

      <?php
    }
  }
}