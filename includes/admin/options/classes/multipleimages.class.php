<?php
/**
 * Multiple Images Component.
 *
 * @package    CFieldTheme
 * @subpackage CFieldTheme
 * @since      0.1.0
 */

if ( !class_exists( 'CFieldTheme_Option_multipleimages' )) {
  class CFieldTheme_Option_multipleimages
  {
    public $type = 'multipleimages';

    public function render_content( $value, $option )
    {
      $token = md5( uniqid( rand(), true ) );

      if ($value) {
        $valueObject = json_decode( $value, true );
        $list        = $valueObject['images'];
        foreach ($list as $key => $item) {
          $image_url                          = CFieldTheme_Image::get_img_by_id( $item['id'] );
          $valueObject['images'][$key]['url'] = $image_url;
        }
        $value = json_encode( $valueObject );
      }
      ?>

      <div ng-controller="multipleImagesController as self">

        <div class="row">
          <div class="col-xs-12 col-md-12">
            <label><?php echo $option['title']; ?></label>
            <br/>
            <input type="button" value="<?php _cftheme_e( 'Upload Images' ); ?>" ng-click="self.upload()"
                   class="btn btn-default btn-sm"/>
            <label>
              <input type="checkbox" ng-model="self.value.options.show"> <?php _cftheme_e( 'Show?' ); ?>
            </label>
            <label>
              <input type="checkbox" ng-model="self.value.options.show_in_post"> <?php _cftheme_e( 'Show in Post?' ); ?>
            </label>
          </div>

          <div class="col-xs-12 col-md-12">
            <br/>

            <cf ng-repeat="item in self.value.images">
              <span style="position: absolute;cursor: pointer;">
                <span class="label label-default" ng-click="self.left($index, item)"> < </span>
                <span class="label label-default" ng-click="self.right($index, item)"> > </span>
                <span class="label label-danger" ng-click="self.remove($index)">X</span>
              </span>
              <img
                class="img-thumbnail"
                alt="140x140"
                ng-src="{{ item.url }}"
                style="width: 140px; height: 140px; margin: 1px;">
            </cf>
          </div>

          <div class="col-xs-12 col-md-12">
            <span class="pull-right"
                  title="<?php _cftheme_e( 'Show json code' ) ?>"
                  aria-hidden="true"
                  ng-click="self.showJson = true"
                  style="cursor: pointer;"><></span>

              <textarea ng-show="self.showJson"
                        ng-init="self.showJson = false"
                        name="<?php echo $option['key']; ?>"
                        class="form-control"
                        rows="3">{{ self.value }}
              </textarea>

            <p class="help-block"><?php echo $option['description']; ?></p>
          </div>
        </div>

      </div>

      <!-- ------------------------------------------- -->

      <script>
        angular.module("builderOptionsApp")
          .controller('multipleImagesController', ['$scope', function ($scope) {
            var $self = this;

            var value = '<?php echo $value; ?>';
            if (value) {
              $self.value = angular.fromJson(value);
            } else {
              $self.value = {
                options: {
                  show        : false,
                  show_in_post: false
                },
                images : []
              };
            }

            $self.upload = function () {
              var custom_uploader = wp.media({
                title   : '<?php _cftheme_e( 'Multiple Featured Images' ); ?>',
                button  : {
                  text: '<?php _cftheme_e( 'Insert' ); ?>'
                },
                multiple: true,
                library : {type: 'image'}
              })
                .on('select', function () {
                  var attachment = custom_uploader.state().get('selection').toJSON();

                  angular.forEach(attachment, function (value) {
                    $self.value.images.push({
                      id : value.id,
                      url: value.url
                    });

                    if (!$scope.$$phase) {
                      $scope.$apply();
                    }
                  });
                })
                .open();
            };

            $self.left = function (index, item) {
              if (index != 0) {
                var before = $self.value.images[index - 1];
                $self.value.images[index - 1] = angular.copy(item);
                $self.value.images[index] = angular.copy(before);
              }
            };

            $self.right = function (index, item) {
              var total = $self.value.images.length;

              if ((index + 1) != total) {
                var after = $self.value.images[index + 1];
                $self.value.images[index + 1] = angular.copy(item);
                $self.value.images[index] = angular.copy(after);
              }
            };

            $self.remove = function (index) {
              $self.value.images.splice(index, 1);
            };
          }])
      </script>

      <?php
    }
  }
}