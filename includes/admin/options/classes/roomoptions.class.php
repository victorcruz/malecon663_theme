<?php
/**
 * Room Options Component.
 *
 * @package    CFieldTheme
 * @subpackage CFieldTheme
 * @since      0.1.0
 */

if ( !class_exists( 'CFieldTheme_Option_roomoptions' )) {
  class CFieldTheme_Option_roomoptions
  {
    public $type = 'roomoptions';

    public function render_content( $value, $option )
    {
      $token = md5( uniqid( rand(), true ) );
      ?>

      <div class="row" ng-controller="roomController as self">

        <div class="col-md-12">
          <label><?php echo $option['title']; ?></label>
          <br/>
          <button ng-show="showBtnCreate" ng-init="showBtnCreate = true;" type="button"
                  class="btn btn-default btn-sm"
                  ng-click="self.show(); showBtnRemove = true; showBtnCreate = false;"><?php _cftheme_e(
              'Add New Service'
            ) ?></button>
          <button ng-show="showBtnRemove" ng-init="showBtnRemove = false;" type="button" class="btn btn-default btn-sm"
                  ng-click="showBtnRemove = false; showBtnCreate = true; self.showAdd = false;"><?php _cftheme_e(
              'Close'
            ) ?></button>
          <br/><br/>

          <div ng-show="self.showAdd">
            <div class="row">
              <div class="col-md-10">
                <div class="form-group">
                  <label class="sr-only"><?php _cftheme_e( 'Name of Service, ex: Wifi' ) ?></label>
                  <input type="text"
                         ng-enter="self.addService(service); service = {}; self.showAdd = false;showBtnRemove = false; showBtnCreate = true;"
                         ng-model="service.name"
                         class="form-control"
                         placeholder="<?php _cftheme_e( 'Name of Service, ex: Wifi' ) ?>"
                    >
                </div>
                <div class="form-group">
                  <label class="sr-only"><?php _cftheme_e( 'Description of Service' ) ?></label>
                <textarea class="form-control"
                          ng-model="service.description"
                          rows="3"
                          placeholder="<?php _cftheme_e( 'Description of Service' ) ?>"></textarea>
                </div>
              </div>

              <div class="col-md-2">
                <button type="button" class="btn btn-default btn-lg btn-block"
                        ng-click="self.addService(service); service = {}; self.showAdd = false;showBtnRemove = false; showBtnCreate = true;">
                  <?php _cftheme_e( 'Create' ) ?>
                </button>
              </div>
            </div>
          </div>

          <div class="panel-group" role="tablist" aria-multiselectable="true">
            <div class="panel panel-default" ng-repeat="servi in self.services">
              <div class="panel-heading" role="tab">
                <h4 class="panel-title">
                  <a role="button" data-toggle="collapse" href=".{{ $index }}"
                     aria-expanded="false" aria-controls="{{ $index }}">
                    {{ servi.name }}
                  </a>
                  <button type="button" class="btn btn-danger btn-xs pull-right"
                          ng-click="self.removeItem($index)"><?php _cftheme_e( 'Remove' ) ?></button>
                </h4>
              </div>
              <div class="{{ $index }} panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                <div class="panel-body">
                  <div class="form-group">
                    <label class="sr-only"><?php _cftheme_e( 'Name of Service, ex: Wifi' ) ?></label>
                    <input type="text"
                           ng-model="servi.name"
                           class="form-control"
                           placeholder="<?php _cftheme_e( 'Name of Service, ex: Wifi' ) ?>"
                      >
                  </div>
                  <div class="form-group">
                    <label class="sr-only"><?php _cftheme_e( 'Description of Service' ) ?></label>
                        <textarea class="form-control"
                                  ng-model="servi.description"
                                  rows="3"
                                  placeholder="<?php _cftheme_e( 'Description of Service' ) ?>"></textarea>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <span class="pull-right"
                title="<?php _cftheme_e( 'Show json code' ) ?>"
                aria-hidden="true"
                ng-click="self.showJson = true"
                style="cursor: pointer;"><></span>
          <textarea ng-show="self.showJson"
                    ng-init="self.showJson = false"
                    name="<?php echo $option['key']; ?>"
                    class="form-control"
                    rows="3"
                    style="display: block;">{{ self.services }}
        </textarea>
        </div>

      </div>

      <!-- ------------------------------------------- -->

      <script>
        angular.module("builderOptionsApp")
          .controller('roomController', [function () {
            var $self = this;
            $self.showAdd = false;

            var value = '<?php echo $value; ?>';
            if (value) {
              $self.services = angular.fromJson(value);
            } else {
              $self.services = [];
            }

            $self.show = function () {
              $self.showAdd = true;
            };

            $self.addService = function (service) {
              if (!!service.name) {
                $self.services.push(service);
              }
            };

            $self.removeItem = function (index) {
              $self.services.splice(index, 1);
            };
          }])
          .directive('ngEnter', [function () {
            return function (scope, element, attrs) {
              element.bind("keydown keypress", function (event) {
                if (event.which === 13) {
                  scope.$apply(function () {
                    scope.$eval(attrs.ngEnter);
                  });

                  event.preventDefault();
                }
              });
            }
          }])
      </script>

      <?php
    }
  }
}