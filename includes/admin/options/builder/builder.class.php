<?php

/**
 * Options Builder.
 *
 * @package    CFieldTheme
 * @subpackage CFieldTheme
 * @since      0.1.0
 */
class CFieldTheme_Options_Builder
{

  /**
   * Options to generate.
   *
   * @static
   * @var string
   */
  static $options;

  /**
   * Type posts to show.
   *
   * @static
   * @var string
   */
  static $post_types;

  /**
   * Hook into the appropriate actions when the class is constructed.
   */
  public function __construct( $post_types, $options )
  {
    self::$post_types = $post_types;
    self::$options    = $options;

    add_action( 'add_meta_boxes', [ $this, 'add_meta_box' ] );
    add_action( 'save_post', [ $this, 'save' ] );

    $theme_dir = CFieldTheme::$theme_includes_dir.'admin/options/classes/';

    // Include all options classes.
    // Include all .php files into ./includes/admin/options/classes/classes folder.
    foreach (glob( $theme_dir.'*.php' ) as $filename) {
      include_once( $filename );
    }
  }

  /**
   * Adds the meta box container.
   */
  public function add_meta_box( $post_type )
  {
    if (in_array( $post_type, self::$post_types )) {
      add_meta_box(
        'cfieldtheme_meta_box',                                 // $id
        _cftheme__( 'Options' ),                                // $title
        [ $this, 'render_meta_box_content' ],                   // $callback
        $post_type,                                             // $post_type
        'normal',                                               // $context
        'high'                                                  // $priority
      );
    }
  }

  /**
   * Save the meta when the post is saved.
   *
   * @param $post_id The ID of the post being saved.
   *
   * @return mixed
   */
  public function save( $post_id )
  {

    /*
     * We need to verify this came from the our screen and with proper authorization,
     * because save_post can be triggered at other times.
     */

    // Check if our nonce is set.
    if ( !isset( $_POST['cfieldtheme_custom_box_nonce'] )) {
      return $post_id;
    }

    $nonce = $_POST['cfieldtheme_custom_box_nonce'];

    // Verify that the nonce is valid.
    if ( !wp_verify_nonce( $nonce, 'cfieldtheme_custom_box' )) {
      return $post_id;
    }

    // If this is an autosave, our form has not been submitted,
    //     so we don't want to do anything.
    if (defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE) {
      return $post_id;
    }

    // Check the user's permissions.
    if ('page' == $_POST['post_type']) {

      if ( !current_user_can( 'edit_page', $post_id )) {
        return $post_id;
      }

    } else {

      if ( !current_user_can( 'edit_post', $post_id )) {
        return $post_id;
      }
    }

    foreach (self::$options as $option) {
      $data = sanitize_text_field( $_POST[$option['key']] );

      update_post_meta( $post_id, $option['key'], $data );
    }
  }


  /**
   * Render Meta Box content.
   *
   * @param WP_Post $post The post object.
   */
  public function render_meta_box_content( $post )
  {

    // Add an nonce field so we can check for it later.
    wp_nonce_field( 'cfieldtheme_custom_box', 'cfieldtheme_custom_box_nonce' );

    echo '<script>angular.module("builderOptionsApp", [])</script>';
    echo '<div ng-app="builderOptionsApp" class="cf-bootstrap-wrapper">';
    foreach (self::$options as $option) {
      if (in_array( get_post_type(), $option['post_types'] ) || !$option['post_types']) {
        // Retrieve an existing value from the database.
        $value = get_post_meta( $post->ID, $option['key'], true );

        // Load type component and render it content.
        $classComponent = new $option['class']( $post->ID );

        echo '<div id="'.$option['key'].'" class="cfieldtheme-component panel panel-default"><div class="panel-body">';
        $classComponent->render_content( $value, $option );
        echo '</div></div>';
      }
    }
    echo '</div>';
  }
}