<?php
/**
 * Options functionality.
 * Options of Page, Post and Other Type Posts
 *
 * @package    CFieldTheme
 * @subpackage CFieldTheme
 * @since      0.1.0
 */

$theme_dir = CFieldTheme::$theme_includes_dir . 'admin/options/builder/';

// Include Builder.
include($theme_dir . 'builder.class.php');

//--------------------------------------------------------------------------------

// Type posts to show.
$post_types = [
    'post',
    'page'
];

// Options.
$options = [

  //-------------------------------------------------------------------------------
  // Type inputtext.
  //  [
  //    'title'       => _cftheme__( 'Testing Input Text' ),
  //    'key'         => 'cftheme_inputtext_test',
  //    'class'       => 'CFieldTheme_Option_input',
  //    'type_input'  => 'text',
  //    'description' => _cftheme__( 'This is a description' )
  //  ],
  //-------------------------------------------------------------------------------
  // Columns Sidebars.
    [
        'title'   => _cftheme__('Style Sidebar'),
        'key'     => 'cftheme_column_sidebar',
        'class'   => 'CFieldTheme_Option_columns',
        'options' => [
            '1' => ['value' => 'right-sidebar', 'image' => CFieldTheme::$theme_url . '/images/sidebars/right-sidebar.png'],
            '2' => ['value' => 'left-sidebar', 'image' => CFieldTheme::$theme_url . '/images/sidebars/left-sidebar.png'],
            '3' => ['value' => 'both-sidebar', 'image' => CFieldTheme::$theme_url . '/images/sidebars/both-sidebar.png'],
            '4' => [
                'value'   => 'no-sidebar',
                'image'   => CFieldTheme::$theme_url . '/images/sidebars/no-sidebar.png',
                'default' => 'selected'
            ]
        ]
    ],
  //-------------------------------------------------------------------------------
  //Type checkbox.
    [
        'title'       => _cftheme__('Show Author'),
        'key'         => 'cftheme_show_author',
        'class'       => 'CFieldTheme_Option_input',
        'type_input'  => 'checkbox',
        'post_types'  => ['post'],
        'description' => _cftheme__('Show information of author.')
    ],
  //-------------------------------------------------------------------------------
  // Select show/hide other options components.
    [
        'title'   => _cftheme__('Select Type of Header'),
        'key'     => 'cftheme_type_header',
        'class'   => 'CFieldTheme_Option_selectoption',
        'options' => [
            '1' => [
                'value' => 'cftheme_image_id_header',
                'name'  => 'Image'
            ],
            '2' => [
                'value' => 'cftheme_slider_header',
                'name'  => 'Slider'
            ]
        ]
    ],
  //-------------------------------------------------------------------------------
  //Type inputtext.
    [
        'title'       => _cftheme__('Slider Header ID'),
        'key'         => 'cftheme_slider_header',
        'class'       => 'CFieldTheme_Option_inputtext',
        'description' => _cftheme__('ID of Revolution Slider to show as header of page.')
    ],
  //-------------------------------------------------------------------------------
  //Type selectimage.
    [
        'title'       => _cftheme__('Simple Image Header'),
        'key'         => 'cftheme_image_id_header',
        'class'       => 'CFieldTheme_Option_selectimage',
        'description' => _cftheme__('This is the image to show in simple header when not used revolution slider.')
    ],
  //-------------------------------------------------------------------------------
  // Select show/hide other options components.
    [
        'title'      => _cftheme__('This page is a Room?'),
        'key'        => 'cftheme_this_page_is_room',
        'class'      => 'CFieldTheme_Option_selectoption',
        'post_types' => ['page'],
        'options'    => [
            '1' => [
                'value' => '',
                'name'  => _cftheme__('No')
            ],
            '2' => [
                'value'  => 'cftheme_room_options',
                'name'   => _cftheme__('Yes'),
                'others' => 'cftheme_price' // separated by comma
            ]
        ]
    ],
  //-------------------------------------------------------------------------------
  //Type roomoptions.
    [
        'title'       => _cftheme__('Room Services'),
        'key'         => 'cftheme_room_options',
        'class'       => 'CFieldTheme_Option_roomoptions',
        'post_types'  => ['page'],
        'description' => _cftheme__('This is de form to add and select options of room.')
    ],
  //-------------------------------------------------------------------------------
  //Type input text.
    [
        'title'       => _cftheme__('Price by night'),
        'key'         => 'cftheme_price',
        'class'       => 'CFieldTheme_Option_input',
        'type_input'  => 'text',
        'post_types'  => ['page'],
        'description' => ''
    ],
  //-------------------------------------------------------------------------------
  //Type checkbox.
    [
        'title'       => _cftheme__('Not Show Featured Image'),
        'key'         => 'cftheme_show_featured_image_in_post',
        'class'       => 'CFieldTheme_Option_input',
        'type_input'  => 'checkbox',
        'post_types'  => ['post'],
        'description' => _cftheme__('Not show Featured Image in single post.')
    ],
  //-------------------------------------------------------------------------------
  //Type checkbox.
    [
        'title'       => _cftheme__('Not Show Featured Image in List'),
        'key'         => 'cftheme_show_featured_image_in_list_posts',
        'class'       => 'CFieldTheme_Option_input',
        'type_input'  => 'checkbox',
        'post_types'  => ['post'],
        'description' => _cftheme__('Not show Featured Image in list of posts.')
    ],
  //-------------------------------------------------------------------------------
  // Multiples Images as Featured Image.
    [
        'title'       => _cftheme__('Multiple Featured Images'),
        'key'         => 'cftheme_feaured_multiple_images',
        'class'       => 'CFieldTheme_Option_multipleimages',
        'post_types'  => ['post'],
        'description' => _cftheme__('Identifying this article with several images instead of a only image.')
    ],
  //-------------------------------------------------------------------------------
  // Select show/hide other options components.
    [
        'title'       => _cftheme__('Page Contact Options'),
        'key'         => 'cftheme_this_is_a_contact_page',
        'class'       => 'CFieldTheme_Option_selectoption',
        'description' => _cftheme__('Is this a page contact?'),
        'post_types'  => ['page'],
        'options'     => [
            '1' => [
                'value' => '',
                'name'  => _cftheme__('No')
            ],
            '2' => [
                'value'  => 'cftheme_shortcode_contact',
                'name'   => _cftheme__('Yes'),
                'others' => ''
            ]
        ]
    ],
  //-------------------------------------------------------------------------------
  //Type input text.
    [
        'title'       => _cftheme__('Shortcode Cotact Form'),
        'key'         => 'cftheme_shortcode_contact',
        'class'       => 'CFieldTheme_Option_input',
        'type_input'  => 'text',
        'post_types'  => ['page'],
        'description' => _cftheme__(
            'This is the shortcode to show de contact form in page, get from <a href="http://www.malecon663.com/wp-admin/admin.php?page=wpcf7">here</a>.'
        )
    ],
  //-------------------------------------------------------------------------------
  //Type input text.
    [
        'title'       => _cftheme__('Show Element When'),
        'key'         => 'cftheme_showen_yeoman',
        'class'       => 'CFieldTheme_Option_showhen',
        'id'          => 'page_template',
        'select'      => 'template-index-yeo.php',
        'show'        => 'cftheme_index_data,cftheme_show_intro,cftheme_show_automatic_intro',
        'description' => _cftheme__('Show element when click in Page Attributes -> Template: Yeoman Index')
    ],
  //-------------------------------------------------------------------------------
  //Type input text.
    [
        'title'       => _cftheme__('Index Logo'),
        'key'         => 'cftheme_index_data',
        'class'       => 'CFieldTheme_Option_yeoindex',
        'description' => _cftheme__('Index with logo animation.')
    ],
  //-------------------------------------------------------------------------------
  //Type checkbox.
    [
        'title'       => _cftheme__('Show Intro'),
        'key'         => 'cftheme_show_intro',
        'class'       => 'CFieldTheme_Option_input',
        'type_input'  => 'checkbox',
        'description' => _cftheme__('Show content with logo.')
    ],
  //-------------------------------------------------------------------------------
  //Type checkbox.
    [
        'title'       => _cftheme__('Show Automatic Intro'),
        'key'         => 'cftheme_show_automatic_intro',
        'class'       => 'CFieldTheme_Option_input',
        'type_input'  => 'checkbox',
        'description' => _cftheme__('Show content after logo animation.')
    ],
  //-------------------------------------------------------------------------------
  //Type page sound.
    [
        'title'       => _cftheme__('Page Sound'),
        'key'         => 'cftheme_page_sound',
        'class'       => 'CFieldTheme_Option_pagesound',
        'description' => ''
    ],
  //-------------------------------------------------------------------------------
//  //Type page sound.
//    [
//        'title'       => _cftheme__('Next Opening'),
//        'key'         => 'cftheme_next_opening',
//        'class'       => 'CFieldTheme_Option_nextopening',
//        'description' => ''
//    ],
//-------------------------------------------------------------------------------
  // Select show/hide other options components.
    [
        'title'       => _cftheme__('Next Opening Message Show?'),
        'key'         => 'cftheme_next_opening_message_show',
        'class'       => 'CFieldTheme_Option_selectoption',
        'description' => '',
      //'post_types'  => ['page'],
        'options'     => [
            '1' => [
                'value' => '',
                'name'  => _cftheme__('No')
            ],
            '2' => [
                'value'  => 'cftheme_next_opening_message',
                'name'   => _cftheme__('Yes'),
                'others' => ''
            ]
        ]
    ],
  //-------------------------------------------------------------------------------
  //Type textarea.
    [
        'title'       => _cftheme__('Next Opening Message Text'),
        'key'         => 'cftheme_next_opening_message',
        'class'       => 'CFieldTheme_Option_input',
        'type_input'  => 'textarea',
        //'post_types'  => ['post'],
        'description' => ''
    ],
];

new CFieldTheme_Options_Builder($post_types, $options);