<?php

/**
 * Customizer: CFieldTheme_Customizer_Columns_Control.
 *
 * Example:
 * $wp_customize->add_setting(
 *    'footer_columns',
 *    [
 *      'default'   => '',
 *      'transport' => 'postMessage'
 *    ]
 * );
 *
 * $wp_customize->add_control(
 *    new CFieldTheme_Customizer_Columns_Control(
 *      $wp_customize, 'footer_columns', [
 *        'label'    => _cftheme__( 'Footer Columns Style' ),
 *        'section'  => 'page_style',
 *        'settings' => 'footer_columns',
 *      ]
 *    )
 * );
 *
 * @package    CFieldTheme
 * @subpackage CFieldTheme
 * @since      0.1.0
 */
class CFieldTheme_Customizer_Columns_Control extends WP_Customize_Control
{
  public $type = 'columns';

  public function render_content()
  {
    $token = md5( uniqid( rand(), true ) );
    $id    = $this->id;
    ?>
    <label id="<?php echo $token; ?>">
      <span class="customize-control-title"><?php echo esc_html( $this->label ); ?></span>
      <input type="hidden" <?php $this->link(); ?> value="<?php echo $this->value(); ?>"/>

      <div>
        <img class="cf-to-select" src="<?php echo CFieldTheme::$theme_url.'/images/columns/footer-style1.png' ?>"
             data-column="3,3,3,3"/>
        <img class="cf-to-select" src="<?php echo CFieldTheme::$theme_url.'/images/columns/footer-style2.png' ?>"
             data-column="6,3,3"/>
        <img class="cf-to-select" src="<?php echo CFieldTheme::$theme_url.'/images/columns/footer-style3.png' ?>"
             data-column="3,3,6"/>
        <img class="cf-to-select" src="<?php echo CFieldTheme::$theme_url.'/images/columns/footer-style4.png' ?>"
             data-column="4,4,4"/>
        <img class="cf-to-select" src="<?php echo CFieldTheme::$theme_url.'/images/columns/footer-style5.png' ?>"
             data-column="8,4"/>
        <img class="cf-to-select" src="<?php echo CFieldTheme::$theme_url.'/images/columns/footer-style6.png' ?>"
             data-column="4,8"/>
      </div>

    </label>

    <script>
      /**
       * Access to API Customize.
       *
       * Example:
       * api.instance('footer_columns').get()         Get api data of field.
       * api.instance('footer_columns').set(value)    Get api data of field and change value.
       */
      var api = wp.customize;

      var $elements = jQuery('#<?php echo $token; ?>');
      var $columSelect = $elements.find('img');
      var $input = $elements.find('input');

      var originalValue = api.instance('<?php echo $id; ?>').get();
      if (originalValue) {
        $columSelect.each(function () {
          var $thisImg = jQuery(this);
          var value = $thisImg.attr('data-column');

          if (value == originalValue) {
            $thisImg.addClass('cf-select');
          }
        });
      }

      $columSelect.bind('click', function (e) {
        var $this = jQuery(this);
        var value = $this.attr('data-column');

        $columSelect.removeClass('cf-select');
        api.instance('<?php echo $id; ?>').set(value);
        $this.addClass('cf-select');
        e.stopPropagation();
      });
    </script>

    <style>
      .cf-to-select {
        width : 47%;
      }

      .cf-select {
        width  : 46%;
        border : 1px solid #73AECD;
      }
    </style>
    <?php
  }
}