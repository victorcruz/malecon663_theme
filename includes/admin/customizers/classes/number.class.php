<?php

/**
 * Customizer: CFieldTheme_Customizer_Number_Control.
 *
 * @package    CFieldTheme
 * @subpackage CFieldTheme
 * @since      0.1.0
 */
class CFieldTheme_Customizer_Number_Control extends WP_Customize_Control
{
  public $type = 'number';

  public function render_content()
  {
    ?>
    <label>
      <p id="test">OK SELÑORES</p>
      <span class="customize-control-title"><?php echo esc_html( $this->label ); ?></span>
      <input type="number" <?php $this->link(); ?> value="<?php echo $this->value(); ?>"/>
    </label>
    <script>

    </script>
  <?php
  }
}