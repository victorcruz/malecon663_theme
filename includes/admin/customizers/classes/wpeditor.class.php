<?php

/**
 * Customizer: CFieldTheme_Text_Editor_Custom_Control.
 *
 * @package    CFieldTheme
 * @subpackage CFieldTheme
 * @since      0.1.0
 */
class CFieldTheme_Text_Editor_Custom_Control extends WP_Customize_Control{
  public $type = 'wpeditor';

  public function render_content() {
    ?>
    <label>
      <span class="customize-text_editor"><?php echo esc_html( $this->label ); ?></span>
      <?php
      $settings = [
        'textarea_name' => $this->id
      ];
      wp_editor( $this->value(), $this->id, $settings );
      ?>
    </label>
    <?php
  }
}
?>