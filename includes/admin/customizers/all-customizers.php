<?php
/**
 * Customizer functionality
 *
 * @package    CFieldTheme
 * @subpackage CFieldTheme
 * @since      0.1.0
 */

function cfieldtheme_customize_register( $wp_customize )
{
  $theme_dir = CFieldTheme::$theme_includes_dir.'admin/customizers/classes/';

  // Include all options classes.
  // Include all .php files into ./includes/admin/customizers/classes folder.
  foreach (glob( $theme_dir.'*.php' ) as $filename) {
    include_once( $filename );
  }

  /**
   * ==================================================================================================0
   * Page Style.
   * ==================================================================================================0
   */

  $wp_customize->add_section(
    'page_style',
    [
      'title'    => _cftheme__( 'Page Style' ),
      'priority' => 120,
    ]
  );

  // ---------------------------------------------
//  $wp_customize->add_setting(
//    'testing_wp_editor',
//    [
//      'default'   => '',
//      'transport' => 'refresh'
//    ]
//  );
//
//  $wp_customize->add_control(
//    new CFieldTheme_Text_Editor_Custom_Control(
//      $wp_customize, 'testing_wp_editor', [
//        'label'    => _cftheme__( 'WP Editor Testing' ),
//        'section'  => 'page_style',
//        'settings' => 'testing_wp_editor',
//      ]
//    )
//  );

  // ---------------------------------------------
  $wp_customize->add_setting(
    'favicon_image',
    [
      'default'           => CFieldTheme::$theme_bower_url.'favicon.ico',
      'sanitize_callback' => 'esc_url_raw'
    ]
  );

  $wp_customize->add_control(
    new WP_Customize_Image_Control(
      $wp_customize, 'favicon_image', [
        'label'    => _cftheme__( 'Favicon' ),
        'section'  => 'page_style',
        'settings' => 'favicon_image',
      ]
    )
  );

  // ---------------------------------------------
  $wp_customize->add_setting(
    'logo_image',
    [
      'default'           => CFieldTheme::$theme_url.'/images/logo.svg',
      'sanitize_callback' => 'esc_url_raw'
    ]
  );

  $wp_customize->add_control(
    new WP_Customize_Image_Control(
      $wp_customize, 'logo_image', [
        'label'    => _cftheme__( 'Logo' ),
        'section'  => 'page_style',
        'settings' => 'logo_image',
      ]
    )
  );

  // ---------------------------------------------
  $wp_customize->add_setting(
    'simple_header_image',
    [
      'default'           => CFieldTheme::$theme_url.'/images/Malecon663_Panoramica-01.jpg',
      'sanitize_callback' => 'esc_url_raw'
    ]
  );

  $wp_customize->add_control(
    new WP_Customize_Image_Control(
      $wp_customize, 'simple_header_image', [
        'label'    => _cftheme__( 'Simple Header Image' ),
        'section'  => 'page_style',
        'settings' => 'simple_header_image',
      ]
    )
  );

  // ---------------------------------------------
  $wp_customize->add_setting(
      'page_sound',
      [
          'default' => true
      ]
  );

  $wp_customize->add_control(
      'page_sound',
      [
          'label'    => _cftheme__( 'Play sound when starting the page, this is a global setting' ),
          'section'  => 'page_style',
          'settings' => 'page_sound',
          'type'     => 'checkbox'
      ]
  );

  // ---------------------------------------------
  $wp_customize->add_setting(
    'animate_slogan',
    [
      'default' => true
    ]
  );

  $wp_customize->add_control(
    'animate_slogan',
    [
      'label'    => _cftheme__( 'Animated Slogan?' ),
      'section'  => 'page_style',
      'settings' => 'animate_slogan',
      'type'     => 'checkbox'
    ]
  );

  // ---------------------------------------------
  $wp_customize->add_setting(
    'default_date_format',
    [
      'default'   => 'd M y',
      'transport' => 'refresh',
    ]
  );

  $wp_customize->add_control(
    'default_date_format',
    [
      'label'    => _cftheme__( 'Default date format' ),
      'section'  => 'page_style',
      'settings' => 'default_date_format',
    ]
  );

  /**
   * ==================================================================================================0
   * Information.
   * ==================================================================================================0
   */

  $wp_customize->add_section(
    'about_us',
    [
      'title'    => _cftheme__( 'About Us Header' ),
      'priority' => 121,
    ]
  );

  // ---------------------------------------------
  $wp_customize->add_setting(
    'show_languages',
    [
      'default' => true
    ]
  );

  $wp_customize->add_control(
    'show_languages',
    [
      'label'    => _cftheme__( 'Show Languages?' ),
      'section'  => 'about_us',
      'settings' => 'show_languages',
      'type'     => 'checkbox'
    ]
  );

  // ---------------------------------------------
  $wp_customize->add_setting(
    'telephone_number',
    [
      'default'   => '',
      'transport' => 'refresh',
    ]
  );

  $wp_customize->add_control(
    'telephone_number',
    [
      'label'    => _cftheme__( 'Telephone Number' ),
      'section'  => 'about_us',
      'settings' => 'telephone_number',
    ]
  );

  // ---------------------------------------------
  $wp_customize->add_setting(
      'phone_number',
      [
          'default'   => '',
          'transport' => 'refresh',
      ]
  );

  $wp_customize->add_control(
      'phone_number',
      [
          'label'    => _cftheme__( 'Phone Number' ),
          'section'  => 'about_us',
          'settings' => 'phone_number',
      ]
  );

  // ---------------------------------------------
  $wp_customize->add_setting(
    'email_contact',
    [
      'default'   => '',
      'transport' => 'refresh',
    ]
  );

  $wp_customize->add_control(
    'email_contact',
    [
      'label'    => _cftheme__( 'Email Contact' ),
      'section'  => 'about_us',
      'settings' => 'email_contact',
    ]
  );

  // ---------------------------------------------
  $wp_customize->add_setting(
    'address_contact',
    [
      'default'   => '',
      'transport' => 'refresh',
    ]
  );

  $wp_customize->add_control(
    'address_contact',
    [
      'label'    => _cftheme__( 'Address Contact' ),
      'section'  => 'about_us',
      'settings' => 'address_contact',
    ]
  );

  /**
   * ==================================================================================================0
   * Social Networks.
   * ==================================================================================================0
   */

  $wp_customize->add_section(
    'social_networks',
    [
      'title'    => _cftheme__( 'Social Networks' ),
      'priority' => 122,
    ]
  );

  // ---------------------------------------------
  $wp_customize->add_setting(
    'facebook',
    [
      'default'   => '',
      'transport' => 'refresh',
    ]
  );

  $wp_customize->add_control(
    'facebook',
    [
      'label'    => _cftheme__( 'Facebook' ),
      'section'  => 'social_networks',
      'settings' => 'facebook',
    ]
  );

  // ---------------------------------------------
  $wp_customize->add_setting(
    'google_plus',
    [
      'default'   => '',
      'transport' => 'refresh',
    ]
  );

  $wp_customize->add_control(
    'google_plus',
    [
      'label'    => _cftheme__( 'Google Plus' ),
      'section'  => 'social_networks',
      'settings' => 'google_plus',
    ]
  );

  // ---------------------------------------------
  $wp_customize->add_setting(
    'twitter',
    [
      'default'   => '',
      'transport' => 'refresh',
    ]
  );

  $wp_customize->add_control(
    'twitter',
    [
      'label'    => _cftheme__( 'Twitter' ),
      'section'  => 'social_networks',
      'settings' => 'twitter',
    ]
  );

  // ---------------------------------------------
  $wp_customize->add_setting(
    'pinterest',
    [
      'default'   => '',
      'transport' => 'refresh',
    ]
  );

  $wp_customize->add_control(
    'pinterest',
    [
      'label'    => _cftheme__( 'Pinterest' ),
      'section'  => 'social_networks',
      'settings' => 'pinterest',
    ]
  );

  // ---------------------------------------------
  $wp_customize->add_setting(
    'instagram',
    [
      'default'   => '',
      'transport' => 'refresh',
    ]
  );

  $wp_customize->add_control(
    'instagram',
    [
      'label'    => _cftheme__( 'Instagram' ),
      'section'  => 'social_networks',
      'settings' => 'instagram',
    ]
  );

  // ---------------------------------------------
  $wp_customize->add_setting(
    'youtube',
    [
      'default'   => '',
      'transport' => 'refresh',
    ]
  );

  $wp_customize->add_control(
    'youtube',
    [
      'label'    => _cftheme__( 'Youtube' ),
      'section'  => 'social_networks',
      'settings' => 'youtube',
    ]
  );

  /**
   * ==================================================================================================0
   * Footer.
   * ==================================================================================================0
   */

  $wp_customize->add_section(
    'footer_options',
    [
      'title'    => _cftheme__( 'Footer' ),
      'priority' => 123,
    ]
  );

  // ---------------------------------------------
  $wp_customize->add_setting(
    'footer_columns',
    [
      'default'   => '',
      'transport' => 'refresh'
    ]
  );

  $wp_customize->add_control(
    new CFieldTheme_Customizer_Columns_Control(
      $wp_customize, 'footer_columns', [
        'label'    => _cftheme__( 'Footer Columns Style' ),
        'section'  => 'footer_options',
        'settings' => 'footer_columns',
      ]
    )
  );

  // ---------------------------------------------
  $wp_customize->add_setting(
    'is_under_construction',
    [
      'default' => false
    ]
  );

  $wp_customize->add_control(
    'is_under_construction',
    [
      'label'    => _cftheme__( 'Is Under Construction?' ),
      'section'  => 'footer_options',
      'settings' => 'is_under_construction',
      'type'     => 'checkbox'
    ]
  );

  // ---------------------------------------------
  $wp_customize->add_setting(
    'show_hrg',
    [
      'default' => true
    ]
  );

  $wp_customize->add_control(
    'show_hrg',
    [
      'label'    => _cftheme__( 'Show HrG Arquitectura?' ),
      'section'  => 'footer_options',
      'settings' => 'show_hrg',
      'type'     => 'checkbox'
    ]
  );

  // ---------------------------------------------
  $wp_customize->add_setting(
    'url_hrg',
    [
      'default'   => 'http://www.habanaregeneracion.com/',
      'transport' => 'refresh',
    ]
  );

  $wp_customize->add_control(
    'url_hrg',
    [
      'label'    => _cftheme__( 'Hrg Arquitectura URL' ),
      'section'  => 'footer_options',
      'settings' => 'url_hrg',
    ]
  );
}

add_action( 'customize_register', 'cfieldtheme_customize_register' );