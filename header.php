<!DOCTYPE HTML>
<html>
<head>
  <?php global $post_meta_array; ?>
  <?php $post_meta_array = get_post_custom(get_the_ID()) ?>

  <meta charset="utf-8">
  <title><?php bloginfo('title'); ?> - <?php the_title() ?></title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
  <link rel="shortcut icon"
        href="<?php echo get_theme_mod('favicon_image', CFieldTheme::$theme_bower_url . 'favicon.ico'); ?>">
  <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <!--<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>-->
  <!--<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>-->
  <![endif]-->

  <!-- Stylesheets -->
  <?php wp_head(); ?>

  <script>
    (function (i, s, o, g, r, a, m) {
      i['GoogleAnalyticsObject'] = r;
      i[r] = i[r] || function () {
            (i[r].q = i[r].q || []).push(arguments)
          }, i[r].l = 1 * new Date();
      a = s.createElement(o),
          m = s.getElementsByTagName(o)[0];
      a.async = 1;
      a.src = g;
      m.parentNode.insertBefore(a, m)
    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

    ga('create', 'UA-66852925-1', 'auto');
    ga('send', 'pageview');

  </script>
</head>

<body>

<!-- Top header -->
<div id="top-header">
  <div class="container">

    <?php
    $page_sound = json_decode(
        $post_meta_array['cftheme_page_sound'][0],
        true
    );

    if ($page_sound['image']['id']) {
      $page_sound_image_url = CFieldTheme_Image::get_img_by_id($page_sound['image']['id']);
    }

    if ($page_sound['sound']['id']) {
      $page_sound_url = CFieldTheme_Utils::get_url_by_id($page_sound['sound']['id']);
    }
    ?>

    <?php if (get_theme_mod('page_sound')): ?>
      <?php if ($page_sound['play_sound']): ?>
        <?php if ($page_sound_url != ''): ?>
          <div id="cf-box-audio-player"
               class="cf-show-audio-player"
               data-autoplay="<?php echo $page_sound['options']['autoplay'] ?>"
               data-automatic_time_close_media="<?php echo $page_sound['options']['automatic_time_close_media'] ?>"
               data-automatic_show_player="<?php echo $page_sound['options']['automatic_show_player'] ?>">
            <i class="fa icon-remove pull-right"></i>

            <div class="cf-div">
<!--              <img src="--><?php //echo $page_sound_image_url ?><!--"/>-->
              <button id="cf-page-audio-play" class="btn btn-xs btn-control">
                <i class="fa icon-play-circle media-player-icon"></i>
              </button>
              <button id="cf-page-audio-pause" class="btn btn-xs btn-control">
                <i class="fa icon-pause media-player-icon"></i>
              </button>
              <audio id="cf-page-audio" src="<?php echo $page_sound_url ?>" loop
                     <?php if ($page_sound['options']['autoplay']): ?>autoplay<?php endif ?>></audio>
            </div>

<!--            <div class="cf-div right">-->
<!--              <h5>--><?php //echo $page_sound['options']['author_name'] ?><!--</h5>-->
<!---->
<!--              <p>--><?php //echo $page_sound['options']['title'] ?><!--</p>-->
<!--            </div>-->
          </div>
        <?php endif; ?>
      <?php endif; ?>
    <?php endif; ?>

    <div class="row">
      <div class="col-xs-6">
        <div class="th-text pull-left">
          <?php if (get_theme_mod('page_sound')): ?>
            <?php if ($page_sound['play_sound']): ?>
              <?php if ($page_sound_url != ''): ?>
                <div class="th-item">
                  <i id="cf-show-audio-player" class="fa fa-play" style="cursor:pointer;"></i>
                </div>
              <?php endif; ?>
            <?php endif; ?>
          <?php endif; ?>

          <?php if (get_theme_mod('telephone_number')): ?>
            <div class="th-item">
              <a href="#">
                <i class="fa fa-phone"></i> <?php echo get_theme_mod('telephone_number'); ?>
              </a>
            </div>
          <?php endif; ?>

          <?php if (get_theme_mod('phone_number')): ?>
            <div class="th-item">
              <a href="#">
                <i class="fa fa-mobile-phone"></i> <?php echo get_theme_mod('phone_number'); ?>
              </a>
            </div>
          <?php endif; ?>

          <?php if (get_theme_mod('email_contact')): ?>
            <div class="th-item">
              <a href="mailto:<?php echo get_theme_mod('email_contact'); ?>">
                <i class="fa fa-envelope"></i> <?php echo get_theme_mod('email_contact'); ?>
              </a>
            </div>
          <?php endif; ?>
        </div>
      </div>
      <div class="col-xs-6">
        <div class="th-text pull-right">
          <?php if (get_theme_mod('show_languages', false)): ?>
            <div class="th-item">
              <div class="btn-group cf-languages">
                  <?php
                  if (function_exists('pll_the_languages')) {
                    $args = [
                        'show_flags' => 1,
                        'force_home' => 0
                    ];
                    pll_the_languages($args);
                  }
                  ?>
              </div>
            </div>
          <?php endif; ?>
          <div class="th-item">
            <div class="social-icons">
              <?php if (get_theme_mod('facebook')): ?>
                <a href="<?php echo get_theme_mod('facebook'); ?>" title="Facebook"><i
                      class="fa fa-facebook"></i></a>
              <?php endif; ?>

              <?php if (get_theme_mod('google_plus')): ?>
                <a href="<?php echo get_theme_mod('google_plus'); ?>" title="Google Plus"><i
                      class="fa fa-google-plus-square"></i></a>
              <?php endif; ?>

              <?php if (get_theme_mod('twitter')): ?>
                <a href="<?php echo get_theme_mod('twitter'); ?>" title="Twitter"><i
                      class="fa fa-twitter"></i></a>
              <?php endif; ?>

              <?php if (get_theme_mod('pinterest')): ?>
                <a href="<?php echo get_theme_mod('pinterest'); ?>" title="Pinterest"><i
                      class="fa fa-pinterest-square"></i></a>
              <?php endif; ?>

              <?php if (get_theme_mod('instagram')): ?>
                <a href="<?php echo get_theme_mod('instagram'); ?>" title="Instagram"><i
                      class="fa fa-instagram"></i></a>
              <?php endif; ?>

              <?php if (get_theme_mod('youtube')): ?>
                <a href="<?php echo get_theme_mod('youtube'); ?>" title="Youtube"><i
                      class="fa fa-youtube-play"></i></a>
              <?php endif; ?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Header Mobile Phone -->
<?php if (wp_is_mobile()): ?>
  <header class="visible-xs">
    <!-- Navigation -->
    <div class="navbar yamm navbar-default" id="sticky">
      <div class="container">
        <div class="navbar-header">
          <button type="button" data-toggle="collapse" data-target="#navbar-collapse-grid"
                  class="navbar-toggle"><span class="icon-bar"></span> <span class="icon-bar"></span>
            <span class="icon-bar"></span></button>

          <!-- Logo -->
          <div id="logo">
            <div class="logo-box mobile">
              <a href="<?php bloginfo('url'); ?>/">
                <img src="<?php echo get_theme_mod('logo_image',
                    CFieldTheme::$theme_bower_url . 'images/logo.svg'); ?>"/>
              </a>
            </div>
            <div class="slogan-mobile">
              <a class="slogan-textillate slogan-menu" style="position: absolute;"
                 data-slogan-animate="<?php echo get_theme_mod('animate_slogan', true); ?>"
                 href="<?php bloginfo('url'); ?>"><?php bloginfo('description'); ?></a>
            </div>
          </div>
        </div>
        <div id="navbar-collapse-grid" class="navbar-collapse collapse">
          <?php if (has_nav_menu('header-menu-top')) {
            wp_nav_menu([
                'theme_location' => 'header-menu-top',
                'container' => '',
                'container_class' => '',
                'menu_class' => '',
                'menu_id' => '',
                'depth' => 3,
                'walker' => new CFieldTheme_Header_Top_Nav_Menu()
            ]);
          } ?>
        </div>
      </div>
    </div>
  </header>
<?php endif; ?>

<?php
$image_url = get_theme_mod('simple_header_image',
    CFieldTheme::$theme_url . '/images/Malecon663_Panoramica-01.jpg');
$type_header = $post_meta_array['cftheme_type_header'][0];
if ($type_header && $type_header == 'cftheme_image_id_header') {
  if ($post_meta_array[$type_header][0]) {
    $image_url = CFieldTheme_Image::get_img_by_id($post_meta_array[$type_header][0]);
  }
}
?>

<?php if ($type_header == 'cftheme_slider_header'): ?>
  <section id="main-menu" class="visible-sm visible-md visible-lg">
    <div class="navbar-wrapper">
      <div class="navbar navbar-static-top" role="navigation">
        <div class="container" style="position: relative;">
          <div class="logo-box">
            <a href="<?php bloginfo('url'); ?>">
              <img
                  src="<?php echo get_theme_mod('logo_image',
                      CFieldTheme::$theme_bower_url . 'images/logo.svg'); ?>"/>
            </a>
          </div>
          <div class="navbar-header">
            <a class="slogan-textillate slogan-menu navbar-brand"
               data-slogan-animate="<?php echo get_theme_mod('animate_slogan', true); ?>"
               href="<?php bloginfo('url'); ?>"><?php bloginfo('description'); ?></a>
          </div>
          <div class="navbar-collapse collapse">
            <?php if (has_nav_menu('header-menu-top')) {
              wp_nav_menu([
                  'theme_location' => 'header-menu-top',
                  'container' => '',
                  'container_class' => '',
                  'menu_class' => '',
                  'menu_id' => '',
                  'depth' => 3,
                  'walker' => new CFieldTheme_Header_Top_Nav_Menu()
              ]);
            } ?>
          </div>
        </div>
      </div>
    </div>
  </section>
<?php endif; ?>

<?php if ($type_header == 'cftheme_image_id_header'): ?>
  <section id="main-menu" class="visible-sm visible-md visible-lg">
    <div class="navbar-wrapper" style="position: initial;margin-top: 0px;">
      <div class="navbar navbar-static-top exshrink navbar-default" role="navigation">
        <div class="container" style="position: relative;">
          <div class="logo-box exshrink">
            <a href="<?php bloginfo('url'); ?>">
              <img
                  src="<?php echo get_theme_mod('logo_image',
                      CFieldTheme::$theme_bower_url . 'images/logo.svg'); ?>"/>
            </a>
          </div>
          <div class="navbar-header exshrink">
            <a class="slogan-textillate slogan-menu navbar-brand"
               href="<?php bloginfo('url'); ?>"><?php bloginfo('description'); ?></a>
          </div>

          <!-- TODO: VCA: Este bloque es el menu -->
          <div class="navbar-collapse collapse">
            <?php if (has_nav_menu('header-menu-top')) {
              wp_nav_menu([
                  'theme_location' => 'header-menu-top',
                  'container' => '',
                  'container_class' => '',
                  'menu_class' => '',
                  'menu_id' => '',
                  'depth' => 3,
                  'walker' => new CFieldTheme_Header_Top_Nav_Menu()
              ]);
            } ?>
          </div>
        </div>
      </div>
    </div>
  </section>
<?php endif; ?>

<?php
/**
 * Revolution Slider.
 */
if (!is_page_template('template-contact1.php')) {
  $rev_slider = $post_meta_array['cftheme_slider_header'][0];
  if ($type_header == 'cftheme_slider_header' && $rev_slider) {
    putRevSlider($rev_slider);
  }
}
?>

<?php if (!is_page_template('template-contact1.php')): ?>
  <?php if (!$type_header || $type_header == 'cftheme_image_id_header'): ?>
    <section class="parallax-effect" tabindex="5000" style="overflow: hidden; outline: none;">
      <div id="parallax-pagetitle"
           style="height: 203px;background-image: url(<?php echo $image_url ?>); background-position: 50% -68px;">
        <div class="color-overlay">
          <!-- Page title -->
          <div class="container">
            <div class="row">
              <div class="col-sm-12">
                <?php
                if (function_exists('cfieldtheme_custom_breadcrumbs')) {
                  cfieldtheme_custom_breadcrumbs();
                }
                ?>
                <h1><?php the_title() ?></h1>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  <?php endif; ?>
<?php endif; ?>

<!-- TODO: VCA: Podria ser is_home() -->
<!-- CF:weugshObAgs0onJaurdi -->
<?php if ($type_header == 'cftheme_slider_header'): ?>
<span id="cf-scroll-header" style="display: none;">true</span>
<?php endif; ?>