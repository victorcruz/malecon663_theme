<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * e.g., it puts together the home page when no home.php file exists.
 *
 * Learn more: {@link https://codex.wordpress.org/Template_Hierarchy}
 *
 * package     CFieldTheme
 * @subpackage CFieldTheme
 * @since      0.1.0
 */
get_header(); ?>

<div class="container">
  <div class="row">
    <!-- Blog -->
    <section class="blog mt50">
      <div class="col-md-9">
        <!-- Articles -->
        <?php if (have_posts()) : ?>

          <?php
          // Start the loop.
          while (have_posts()) : the_post();

            /*
             * Include the Post-Format-specific template for the content.
             * If you want to override this in a child theme, then include a file
             * called content-___.php (where ___ is the Post Format name) and that will be used instead.
             */
            get_template_part( 'content', get_post_format() );

            // End the loop.
          endwhile;

          ?>

          <!-- Pagination -->
          <div class="text-center mt50">
            <?php
            // Previous/next page navigation.
            the_posts_pagination(
              [
                'prev_text'          => _cftheme__( 'Previous page' ),
                'next_text'          => _cftheme__( 'Next page' ),
                'before_page_number' => '<span class="meta-nav screen-reader-text"> </span>',
                'screen_reader_text' => ' ',
              ]
            );
            ?>
          </div>

          <?php
        // If no content, include the "No posts found" template.
        else :
          get_template_part( 'content', 'none' );
        endif;
        ?>
      </div>
    </section>

    <!-- Aside -->
    <aside class="mt50">
      <div class="col-md-3">
        <?php dynamic_sidebar( 'cfieldtheme-right' ) ?>
      </div>
    </aside>
  </div>
</div>

<?php get_footer(); ?>
